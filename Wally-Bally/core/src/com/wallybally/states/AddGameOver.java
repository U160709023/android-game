package com.wallybally.states;

public interface AddGameOver {
    //ilk oyun, 100 puanı gç,1000 puanı geç,10 bin puanı geç,tüm levelleri aç

    public void CreateAds();
    public void gameEnded();
    public void InGame();
    public void InMenu();
    public void WatchVideo();
    public void ilkOyun();
    public void hundredPoint();
    public void thousandPoint();
    public void ten_thousandPoint();
    public void Unlock_WholeLevels();
    public void GirisYap();
}
