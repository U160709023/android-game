package com.wallybally.states;

import javax.swing.plaf.basic.BasicButtonUI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.wallybally.ImageLoader.ImageLoader;
import com.wallybally.InputHandler.GameOverInput;
import com.wallybally.InputHandler.RecordHandler;

public class GameOverState extends state {

	private StateManager sm;
	private Rectangle ReplayButton;
	private Rectangle WatchVideoButton;
	int Level;

	public Rectangle getWatchVideoButton() {
		return WatchVideoButton;
	}

	public GameOverState(StateManager sm, int Level) {
		super(sm);
		System.out.println("GAMEOVER STATE SYSTEM OUT: LEVEL = "+ Level);
		if(RecordHandler.getCoin()< 10){
			Level = 1;
		}
		else if(RecordHandler.getCoin() > 10 && RecordHandler.getCoin() < 20){
			Level = 2;
		}
		else if(RecordHandler.getCoin() >20 && RecordHandler.getCoin() < 30){
			Level = 3;
		}
		this.Level = Level;
		this.sm = sm;

		WatchVideoButton = new Rectangle(Gdx.graphics.getWidth()/2- Gdx.graphics.getWidth()/4,Gdx.graphics.getHeight()/2.4f,Gdx.graphics.getWidth()/4,Gdx.graphics.getWidth()/10);

		Gdx.input.setInputProcessor(new GameOverInput(this,Level));
		ReplayButton = new Rectangle(Gdx.graphics.getWidth()/2+ Gdx.graphics.getWidth()/6,Gdx.graphics.getHeight()/2.4f,Gdx.graphics.getWidth()/10,Gdx.graphics.getWidth()/10);

	}

	@Override
	public void render(SpriteBatch SB) {
		SB.setProjectionMatrix(camera.combined);
		SB.begin();
		SB.draw(ImageLoader.RepLayReg, Gdx.graphics.getWidth()/2+ Gdx.graphics.getWidth()/6,Gdx.graphics.getHeight()/2.4f,Gdx.graphics.getWidth()/10,Gdx.graphics.getWidth()/10);
		SB.draw(ImageLoader.watch_video,Gdx.graphics.getWidth()/2- Gdx.graphics.getWidth()/4,Gdx.graphics.getHeight()/2.4f,Gdx.graphics.getWidth()/4,Gdx.graphics.getWidth()/10);

		SB.end();
		
		
		SB.begin();
		ImageLoader.font.draw(SB,"Score  " + RecordHandler.getCoin(), Gdx.graphics.getWidth()/2- Gdx.graphics.getWidth()/3,Gdx.graphics.getHeight()/2+Gdx.graphics.getHeight()/10);

		ImageLoader.font.getData().setScale(4,4);

		ImageLoader.font.setColor(Color.BLACK);
		//System.out.println(ImageLoader.font.getLineHeight());
		SB.end();
		//PB.render(SB
	
	}

	

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disposeElements() {
		ReplayButton = null;
	}

	public StateManager getSm() {
		return sm;
	}

	public void setSm(StateManager sm) {
		this.sm = sm;
	}
	public Rectangle getReplayButton() {
		return ReplayButton;
	}

	public void setReplayButton(Rectangle replayButton) {
		ReplayButton = replayButton;
	}
}
