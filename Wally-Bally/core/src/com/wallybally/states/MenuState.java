package com.wallybally.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.wallybally.ImageLoader.ImageLoader;
import com.wallybally.InputHandler.MenuStateInput;
import com.wallybally.InputHandler.PlayStateInput;
import com.wallybally.InputHandler.RecordHandler;

public class MenuState extends state{
		private StateManager sm;
		private Rectangle StartButtonRect;
		private Rectangle LevelButtonRect;
		public int level;
		public static boolean isClickedStarButton =false;

	public MenuState(StateManager sm) {
		 super(sm);
		 this.sm = sm;

		 level = super.Level;
		 StartButtonRect = new Rectangle( Gdx.graphics.getWidth()*(0.375f), Gdx.graphics.getHeight()*(0.739f), Gdx.graphics.getWidth()*0.3f, Gdx.graphics.getHeight()/10);;
		LevelButtonRect= new Rectangle( Gdx.graphics.getWidth()*(0.375f), Gdx.graphics.getHeight()*(0.739f)-StartButtonRect.getHeight(), Gdx.graphics.getWidth()*0.3f, Gdx.graphics.getHeight()/10);

		 Gdx.input.setInputProcessor(new MenuStateInput(this,super.Level));
	 }
	public StateManager getSm() {
		return sm;
	}
	public void setSm(StateManager sm) {
		this.sm = sm;
	}
	public Rectangle getStartButtonRect() {
		return StartButtonRect;
	}
	public void setStartButtonRect(Rectangle startButtonRect) {
		StartButtonRect = startButtonRect;
	}
	public Rectangle getLevelButtonRect() {
		return LevelButtonRect;
	}

	public void setLevelButtonRect(Rectangle levelButtonRect) {
		LevelButtonRect = levelButtonRect;
	}
	@Override
	public void render(SpriteBatch SB) {
		SB.setProjectionMatrix(camera.combined);
		SB.begin();
		SB.draw(ImageLoader.StartButtonReg, Gdx.graphics.getWidth()*(0.375f), Gdx.graphics.getHeight()*(0.739f), Gdx.graphics.getWidth()*0.3f, Gdx.graphics.getHeight()/10);;
		
		SB.end();
		SB.begin();
		SB.draw(ImageLoader.LevelButton, Gdx.graphics.getWidth()*(0.375f), Gdx.graphics.getHeight()*(0.739f)-StartButtonRect.getHeight(), Gdx.graphics.getWidth()*0.3f, Gdx.graphics.getHeight()/10);;

		SB.end();




		SB.begin();
		ImageLoader.font.draw(SB,"Score  " + RecordHandler.getCoin(), Gdx.graphics.getWidth()/2- Gdx.graphics.getWidth()/7,Gdx.graphics.getHeight()/2);
		ImageLoader.font.setColor(Color.FIREBRICK);
		//System.out.println(ImageLoader.font.getLineHeight());
		SB.end();
	}

	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		
	}

    @Override
    public void disposeElements() {
		StartButtonRect = null;
		LevelButtonRect = null;
    }

}
