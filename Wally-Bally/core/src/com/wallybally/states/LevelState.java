package com.wallybally.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.wallybally.ImageLoader.ImageLoader;
import com.wallybally.InputHandler.LevelStateInput;
import com.wallybally.InputHandler.RecordHandler;



import java.util.prefs.BackingStoreException;

public class LevelState extends state {

    private StateManager sm;

    private Rectangle Level1ScreenRect;
    private Rectangle Level2ScreenRect;
    private Rectangle Level3ScreenRect;
    int Level;

    public LevelState(StateManager stateManager, int Level){
        super(stateManager);
        this.sm = stateManager;
    this.Level = Level;

        Gdx.input.setInputProcessor(new LevelStateInput(this, Level));
//x 1/10, y = 1/3

        Level1ScreenRect = new Rectangle(Gdx.graphics.getWidth()/8,Gdx.graphics.getHeight()*(0.7f),Gdx.graphics.getWidth()/5,Gdx.graphics.getWidth()/5);
        Level2ScreenRect = new Rectangle(Gdx.graphics.getWidth()/8+Gdx.graphics.getWidth()/20 + Level1ScreenRect.getWidth(),Gdx.graphics.getHeight()*(0.7f),Gdx.graphics.getWidth()/5,Gdx.graphics.getWidth()/5);
        Level3ScreenRect = new Rectangle(Gdx.graphics.getWidth()/8+Gdx.graphics.getWidth()/20*2+Level2ScreenRect.getWidth()*2,Gdx.graphics.getHeight()*(0.7f),Gdx.graphics.getWidth()/5,Gdx.graphics.getWidth()/5);
        //0-10 lv1 --- 10-20 lv2 -- 20-30 lv 3


    }


    public void render(SpriteBatch SB){
        SB.setProjectionMatrix(camera.combined);
        SB.begin();
        SB.draw(ImageLoader.choseLevelbtn,Gdx.graphics.getWidth()/8,Gdx.graphics.getHeight()*(0.7f),Gdx.graphics.getWidth()/5,Gdx.graphics.getWidth()/5);

        SB.draw(ImageLoader.choseLevelbtn,Gdx.graphics.getWidth()/8+Gdx.graphics.getWidth()/20+ Level1ScreenRect.getWidth(),Gdx.graphics.getHeight()*(0.7f),Gdx.graphics.getWidth()/5,Gdx.graphics.getWidth()/5);
        SB.draw(ImageLoader.choseLevelbtn,Gdx.graphics.getWidth()/8+Gdx.graphics.getWidth()/20*2+Level2ScreenRect.getWidth()*2,Gdx.graphics.getHeight()*(0.7f),Gdx.graphics.getWidth()/5,Gdx.graphics.getWidth()/5);

        SB.end();



    }
    public  void update(float delta) throws BackingStoreException{

    }

    @Override
    public void disposeElements() {

    }

    public StateManager getSm() {
        return sm;
    }

    public void setSm(StateManager sm) {
        this.sm = sm;
    }

    public Rectangle getLevel1ScreenRect() {
        return Level1ScreenRect;
    }

    public void setLevel1ScreenRect(Rectangle level1ScreenRect) {
        Level1ScreenRect = level1ScreenRect;
    }

    public Rectangle getLevel2ScreenRect() {
        return Level2ScreenRect;
    }

    public void setLevel2ScreenRect(Rectangle level2ScreenRect) {
        Level2ScreenRect = level2ScreenRect;
    }

    public Rectangle getLevel3ScreenRect() {
        return Level3ScreenRect;
    }

    public void setLevel3ScreenRect(Rectangle level3ScreenRect) {
        Level3ScreenRect = level3ScreenRect;
    }


}
