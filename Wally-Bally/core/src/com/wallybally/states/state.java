package com.wallybally.states;

import java.util.prefs.BackingStoreException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wallybally.InputHandler.RecordHandler;

public abstract class state {
	protected OrthographicCamera camera;
	protected StateManager sm;
	int Level = 1;
		public state(StateManager sm){
			camera = new OrthographicCamera();
			camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			this.sm = sm;
			System.out.println(RecordHandler.getCoin());
			if(RecordHandler.getCoin()< 10){
				Level = 1;
			}
			else if(RecordHandler.getCoin() > 10 && RecordHandler.getCoin() < 20){
				Level = 2;
			}
			else if(RecordHandler.getCoin() >20){
				Level = 3;
			}
		}

		public abstract  void render(SpriteBatch SB);
		public abstract void update(float delta) throws BackingStoreException;
		public abstract void disposeElements();
		
}
