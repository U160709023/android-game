package com.wallybally.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.awt.GridLayout;
import java.lang.*;
import java.util.prefs.BackingStoreException;

import com.badlogic.gdx.graphics.g3d.utils.BaseAnimationController;
import com.wallybally.game.WallyBally;
import com.wallybally.ImageLoader.*;
import com.wallybally.InputHandler.*;
import com.wallybally.GameObject.*;
public class PlayState extends state {


	public static boolean isGameEnd = false;


	private static float BallsXKord =(Gdx.graphics.getWidth());
	private static float BallsYKord = (float) (Gdx.graphics.getWidth()/9.6);
	
	public boolean RenderBaseBall = false;
//Game objects is allocated here	
	GameArea GA;
	private StateManager sm;
	public boolean CreateBall = false;
	public static int kazanilanpuan = 0;
	GAForLV2 GAFLV2;
	GAFORLV3 GAFLV3;
	int Level;
	public PlayState(StateManager sm, int Level) {
		super(sm);
		this.setSm(sm);
this.Level = Level;

		GA = new GameArea();
		GAFLV2 = new GAForLV2();
		GAFLV3 = new GAFORLV3();
		Gdx.input.setInputProcessor(new PlayStateInput(this,Level));
	//	BaseBall = new MainPlayer((float) (Gdx.graphics.getWidth()-(float) (Gdx.graphics.getWidth()/9.6))/2 ,(float) ((Gdx.graphics.getHeight())-(Gdx.graphics.getHeight()*0.9)) ,(float) (Gdx.graphics.getWidth()/9.6) ,(float) Gdx.graphics.getHeight()/14);
		//Balls = new Balls((float) (Gdx.graphics.getWidth()/2),  BallsXKord,  BallsYKord ,(float) (Gdx.graphics.getWidth()/9.6));
        disposeElements();
	}
	@Override
	public void render(SpriteBatch SB) {
		SB.setProjectionMatrix(camera.combined);

	//	Balls.render(SB);
		//BaseBall.render(SB);
		
		
		

		if(Level == 1 || LevelStateInput.OnWhichLevel == 1) {

			GA.render(SB);


			if(GA.getKalanCan() == 0) {
				isGameEnd = true;
				GAFLV3.destroyElement();
				GAFLV2.destroyElement();
				try {
					RecordHandler.setCoin(GA.getPuan()+RecordHandler.getCoin());
					kazanilanpuan = GA.getPuan();
					} catch (BackingStoreException e) {
					e.printStackTrace();
					}
//

				this.getSm().popState();
				GA.destroyElement();
				GA = null;
				this.getSm().PushState(new GameOverState(this.sm, Level));

	}

		}
		else if(Level == 2 || LevelStateInput.OnWhichLevel == 2){
			GAFLV2.render(SB);


			if(GAFLV2.getKalanCan() == 0) {
				GAFLV3.destroyElement();
				GA.destroyElement();
				try {
					isGameEnd = true;

					RecordHandler.setCoin(GAFLV2.getPuan()+RecordHandler.getCoin());
					kazanilanpuan = GA.getPuan();

				} catch (BackingStoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//
				this.getSm().popState();
				GAFLV2.destroyElement();
				GAFLV2 = null;
				this.getSm().PushState(new GameOverState(this.sm, Level));

			}


		}
		else if(Level == 3 || LevelStateInput.OnWhichLevel == 3){
			GAFLV3.render(SB);
			if(GAFLV3.getKalanCan() == 0) {
				isGameEnd = true;

				GA.destroyElement();
				GAFLV2.destroyElement();

				try {
					RecordHandler.setCoin(GAFLV3.getPuan()+RecordHandler.getCoin());
					kazanilanpuan = GA.getPuan();

				} catch (BackingStoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//
				this.getSm().popState();
				GAFLV3.destroyElement();
				GAFLV3 = null;
				GAFLV2 = null;
				GA = null;
				this.getSm().PushState(new GameOverState(this.sm,Level));

			}





		}

//		if(GAFLV3.getKalanCan() == 0) {
//		try {
//			RecordHandler.setCoin(GAFLV3.getPuan()+RecordHandler.getCoin());
//		} catch (BackingStoreException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		this.getSm().popState();
//		this.getSm().PushState(new GameOverState(this.sm));
//		
//	}
		
		
//
// 		if(GA.getKalanCan() == 0) {
//		try {
//			RecordHandler.setCoin(GA.getPuan()+RecordHandler.getCoin());
//		} catch (BackingStoreException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		this.getSm().popState();
//		this.getSm().PushState(new GameOverState(this.sm));
//		
//	}
		
	
	}

	@Override
	public void update(float delta) throws BackingStoreException {
		//GA.update(delta);
		//GAFLV3.update(delta);
		
		//GAFLV2.update(delta);
	
		
		
		//BaseBall.update(delta);
		//Balls.update(delta);
		if(Level == 1 || LevelStateInput.OnWhichLevel == 1) {
			GA.update(delta);
		}
		else if(Level == 2 || LevelStateInput.OnWhichLevel == 2){
			GAFLV2.update(delta);

		}
		else if(Level == 3 || LevelStateInput.OnWhichLevel == 3){
			GAFLV3.update(delta);

		}
	
	}

    @Override
    public void disposeElements() {
		if(Level == 1 || LevelStateInput.OnWhichLevel == 1) {
			GAFLV2.destroyElement();
			GAFLV3.destroyElement();

		}
		else if(Level == 2 || LevelStateInput.OnWhichLevel == 2){
			GAFLV3.destroyElement();
		    GA.destroyElement();

		}
		else if(Level == 3 || LevelStateInput.OnWhichLevel == 3){

		GAFLV2.destroyElement();
		GA.destroyElement();

		}

	}


    public StateManager getSm() {
		return sm;
	}
	public void setSm(StateManager sm) {
		this.sm = sm;
	}
	
	
	
	
	public void CreateBall() {
	//	BaseBall = new MainPlayer((float) (Gdx.graphics.getWidth()-(float) (Gdx.graphics.getWidth()/9.6))/2 ,(float) ((Gdx.graphics.getHeight())-(Gdx.graphics.getHeight()*0.9)) ,(float) (Gdx.graphics.getWidth()/9.6) ,(float) Gdx.graphics.getHeight()/14);

	}


	public GameArea getGameArea()
	{

		return GA;
	}

	public GAForLV2 getGameArea2()
	{

		return GAFLV2;
	}
	public GAFORLV3 getGameArea3()
	{

		return GAFLV3;
	}

}
