package com.wallybally.states;

import java.util.Stack;
import java.util.prefs.BackingStoreException;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class StateManager {
//peak döndürür
	//pop siler
	private Stack<state> states;
	
	public StateManager() {
		states = new Stack<state>();
	
	
	}
	
	public void render(SpriteBatch SB) {
		
		states.peek().render(SB);
	}

	public void update(float delta) throws BackingStoreException {
		states.peek().update(delta);
	}
	
	public void PushState(state state){
		states.push(state);
	}
	
	public void popState(){
		states.peek().disposeElements();
		states.pop();

		System.out.println("state size:" +states.size());
	}

}

