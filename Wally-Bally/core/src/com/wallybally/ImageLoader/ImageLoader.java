package com.wallybally.ImageLoader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.wallybally.GameObject.MainPlayer;

public class ImageLoader {
	public static Texture text;
	public static TextureRegion TextureRegion;
	
	
	public static Texture HpBall;
	public static TextureRegion HPBallReg;
	
	public static Texture ForBGClear;
	public static TextureRegion ForBGClearREG;
	
	public static Texture BlueBall;
	public static TextureRegion BlueBallReg;

	public static Texture GreenArrow;
	public static TextureRegion GreenArrowReg;
	
	public static Texture BantOfGame;
	public static TextureRegion BantOfGameReg;
	
	public static Texture BlackBall;
	public static TextureRegion BlackballReg;
	
	public static Texture RedBall;
	public static TextureRegion RedBallReg;
	
	public static Texture GreenBall;
	public static TextureRegion GreenBallReg;
	
	public static Texture YellowBall;
	public static TextureRegion YellowBallReg;
	
	public static Texture RedNeonBar;
	public static TextureRegion RedNeonBarReg;
	
	public static BitmapFont font;
	public static Texture fontTexture;
	public static BitmapFont font1;
	public static BitmapFont font2;
	public static Texture RepLay;
	public static TextureRegion RepLayReg;
	
	public static Texture CanGostergesi;
	public static TextureRegion CanGostergesiReg;




	public static Texture LevelButton;



	public static Texture StartButton;
	public static TextureRegion StartButtonReg;
	
	public static Texture Bomb;
	public static TextureRegion BombReg;

	public static Texture watch_video;


	public static Texture Joker;
	
	public static Texture x2Ball;

	public static Texture myBandBlue;

	public static Texture Electiric;
	public static Texture choseLevelbtn;

	public static   ParticleEffect CollisionToMainEffect;

	public static ParticleEffect CollisionToNeonEffect;


	public static void load() {
		CollisionToMainEffect = new ParticleEffect();
		CollisionToMainEffect.load(Gdx.files.internal("ball_c.p"), Gdx.files.internal(""));
//
		CollisionToMainEffect.start();
		CollisionToNeonEffect = new ParticleEffect();
		CollisionToNeonEffect.load(Gdx.files.internal("red_fire_boi.p"), Gdx.files.internal(""));
		CollisionToNeonEffect.start();

		font = new BitmapFont(Gdx.files.internal("font.fnt"));
		fontTexture = new Texture(Gdx.files.internal("font.png"));
		fontTexture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);



		watch_video = new Texture(Gdx.files.internal("watchvideo.png"));


		font1= new BitmapFont(Gdx.files.internal("font.fnt"));
		font2= new BitmapFont(Gdx.files.internal("font.fnt"));

		ForBGClear = new Texture(Gdx.files.internal("BGX.jpg"));
		ForBGClearREG = new TextureRegion(ForBGClear, 0, 0, 1209, 690);
		//ForBGClearREG = new TextureRegion(ForBGClear, 0, 0, 1920, 1200);

		LevelButton = new Texture(Gdx.files.internal("levelbutton.png"));


		
		RedBall = new Texture(Gdx.files.internal("Red.png"));
		RedBallReg = new TextureRegion(RedBall, 0, 0, 720, 720);
		
		BlackBall = new Texture(Gdx.files.internal("Black.png"));
		BlackballReg = new TextureRegion(BlackBall, 0, 0, 720, 720);
		
		
		
		text = new Texture(Gdx.files.internal("beyzbol.png"));
		TextureRegion = new TextureRegion(text, 0,0,1500,1500);	
		
		BlueBall = new Texture(Gdx.files.internal("Mavi.png"));
		BlueBallReg = new TextureRegion(BlueBall, 0, 0, 724, 720);
		BlueBallReg.flip(false,false);
		
		GreenArrow = new Texture(Gdx.files.internal("Arrow.png"));
		GreenArrowReg = new TextureRegion(GreenArrow, 0,0, 1984,1134);
		
		BantOfGame = new Texture(Gdx.files.internal("agac.png"));
		BantOfGameReg = new TextureRegion(BantOfGame, 0, 0, 960,720);
		
		GreenBall = new Texture(Gdx.files.internal("Green.png"));
		GreenBallReg = new TextureRegion(GreenBall, 0, 0, 727, 720);
		
		YellowBall = new Texture(Gdx.files.internal("YELLOW.png"));
		YellowBallReg = new TextureRegion(YellowBall, 0, 0, 724, 720);
		
		RedNeonBar = new Texture(Gdx.files.internal("RedNeonBar.jpg"));
		RedNeonBarReg = new TextureRegion(RedNeonBar, 0, 0, 16, 211);
		
		RepLay = new Texture(Gdx.files.internal("refresh.png"));
		RepLayReg = new TextureRegion(RepLay, 0, 0, 305, 307);
		
		CanGostergesi = new Texture(Gdx.files.internal("CanGostergesi.png"));
		CanGostergesiReg = new TextureRegion(CanGostergesi, 0, 0, 512, 512);
	
		StartButton = new Texture(Gdx.files.internal("START.png"));
		StartButtonReg = new TextureRegion(StartButton, 0, 0, 740, 286);
	
	
		HpBall = new Texture(Gdx.files.internal("HP1.png"));
		HPBallReg = new TextureRegion(HpBall, 0, 0, 412, 412);
		
		
		Bomb = new Texture(Gdx.files.internal("Bomba.png"));
		
		Joker = new Texture(Gdx.files.internal("joker.png"));
		
		x2Ball = new Texture(Gdx.files.internal("x2_reel.png"));

		myBandBlue = new Texture(Gdx.files.internal("myBandBlue.png"));


		Electiric = new Texture(Gdx.files.internal("electricity-png-1.png"));


		choseLevelbtn = new Texture(Gdx.files.internal("choseLevelbtn.jpg"));
	}
	
	public static void dispose() {
		text.dispose();
		BlueBall.dispose();
		font.dispose();
		fontTexture.dispose();
		BantOfGame.dispose();
		Bomb.dispose();
		x2Ball.dispose();
		myBandBlue.dispose();
		CanGostergesi.dispose();
		RepLay.dispose();
		YellowBall.dispose();
		GreenBall.dispose();
		GreenArrow.dispose();
		RedBall.dispose();
		BlackBall.dispose();
		StartButton.dispose();
		Electiric.dispose();
		ForBGClear.dispose();
		LevelButton.dispose();
		choseLevelbtn.dispose();
		font1.dispose();
		font.dispose();
		font2.dispose();
		watch_video.dispose();
	}
}
