package com.wallybally.GameObject;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface GameOBject {
	
	public void render(SpriteBatch SB);
		
	
	
	public void update(float delta);
	public void destroyElement();
}
