package com.wallybally.GameObject;

import java.awt.Image;
import java.lang.ProcessBuilder.Redirect;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Stack;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.particles.influencers.ColorInfluencer.Random;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.BSpline;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.RandomXS128;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.compression.lzma.Base;
import com.wallybally.ImageLoader.*;
import com.wallybally.states.*;
import com.wallybally.GameObject.Band
;
public class GAForLV2 implements GameOBject{
	//1
	//public static PointBands PB;
	private Stack<PointBandsForLV2> PB;
	private Stack<HPBall> HPBall;

	private static ShapeRenderer ClickingArea;
	private static ShapeRenderer LineArrow;
	public MainPlayer BaseBall;
	private Stack<MainPlayer> BaseBall1;
	private Stack<BallsForLV2> Balls;
	private static Rectangle ArrowCreateArea;
	private static float DetectingColorOfBall;
	private static int CountTheTime; //When will next bilardo ball be created?
	public static int RANDOMX;
	private BandForLV2 band;
	private int AtwhichBaseBall;
	private int AtwhichBall;
	private static boolean ClickedOnScreen = false;//Ekrana tıklanıp tıklanmadığını kontrol eder
	private int CarpilanTop;
	private int CarpanTop;
	public static boolean BallCreateGA= false;
	private int AtWhichBallBumpBand;
	private int RANDOMForNeon;
	private int CountTheTime2;
	private int Puan;
	private float CollisionX;
	private float CollisionY;
	private int KalanCan;
	private int DusmeKontrolSayisi;
	private int Level;
	public static int THDGR;
	private boolean X2Ball;
	private int CountTheTime3;
	private int X2BallCount;
	public GAForLV2() {
		//*9/23
		RANDOMForNeon = 6;
		X2Ball = false;
		band = new BandForLV2(0,Gdx.graphics.getHeight() - (Gdx.graphics.getHeight()/10),Gdx.graphics.getWidth()*(8)/(30),Gdx.graphics.getHeight()/10,
						Gdx.graphics.getWidth()*(8)/(30) + Gdx.graphics.getWidth()/9.5f, Gdx.graphics.getHeight() - Gdx.graphics.getHeight()/10, Gdx.graphics.getWidth()*(8)/(31), Gdx.graphics.getHeight()/10,
						((Gdx.graphics.getWidth()*(16)/(30))) + 2 * (Gdx.graphics.getWidth()/9.6f), Gdx.graphics.getHeight() - Gdx.graphics.getHeight()/10, Gdx.graphics.getWidth()*(8)/(30), Gdx.graphics.getHeight()/10);
		CountTheTime3 = 0;
		X2BallCount = 0;
		PB = new Stack<PointBandsForLV2>();
		PB.push(new PointBandsForLV2(RANDOMForNeon));
		setArrowCreateArea(new Rectangle(0, 0, Gdx.graphics.getWidth(), (float) ( Gdx.graphics.getHeight()/3.75)));
		 RANDOMX = 0;
		ClickingArea = new ShapeRenderer();
		// BaseBall = new MainPlayer(0c, 0, 50, 50);
		 BaseBall1 = new Stack<MainPlayer>();
		 
			HPBall = new Stack<HPBall>();

		// BaseBall1.push(new MainPlayer(100, 100, 70, 70));
		 Balls = new Stack<BallsForLV2>();
		  
		  CountTheTime = 0;
		  CountTheTime2 = 301;
		  Puan = 0;
		  KalanCan = 2;
		  DusmeKontrolSayisi = 0;
		
		}
	@Override
	public void render(SpriteBatch SB) {
		band.render(SB);
		if(X2Ball) {
			X2BallCount++;
		}
		
		if(X2BallCount >900) {
			X2Ball = false;
			X2BallCount = 0;
		}
		if(!HPBall.isEmpty()) {
			HPBall.peek().render(SB);
			
			int CountOfHPBall = 0;
//			System.out.println(BaseBall1.size());
			while(CountOfHPBall < HPBall.size()) {
				HPBall.elementAt(CountOfHPBall).render(SB);
				HPBall.elementAt(CountOfHPBall).update(Gdx.graphics.getDeltaTime());
				CountOfHPBall++;
			}
	 	}

	if(!PB.isEmpty()) {
		SB.begin();
		if(KalanCan == 2) {
		SB.draw(ImageLoader.CanGostergesiReg, Gdx.graphics.getWidth()/2.3f, Gdx.graphics.getHeight()/2.5f, Gdx.graphics.getHeight()/32, Gdx.graphics.getHeight()/32);
		SB.draw(ImageLoader.CanGostergesiReg, Gdx.graphics.getWidth()/2.3f + Gdx.graphics.getHeight()/31, Gdx.graphics.getHeight()/2.5f, Gdx.graphics.getHeight()/32, Gdx.graphics.getHeight()/32);
		}
		
		if(KalanCan == 1){
			SB.draw(ImageLoader.CanGostergesiReg, Gdx.graphics.getWidth()/2.3f, Gdx.graphics.getHeight()/2.5f, Gdx.graphics.getHeight()/32, Gdx.graphics.getHeight()/32);
	
		}
		SB.end();
	}

//		SB.begin();
//		
//		ImageLoader.font.draw(SB, ""+ KalanCan, Gdx.graphics.getWidth()/2.5f +(Gdx.graphics.getWidth()/20), Gdx.graphics.getHeight()/2.4f + (ImageLoader.font.getXHeight()));
//		SB.end();
		
		
		SB.begin();
		ImageLoader.font2.draw(SB,"Score  " + Puan, Gdx.graphics.getWidth()/2- Gdx.graphics.getWidth()/7,Gdx.graphics.getHeight()/2);
		ImageLoader.font2.setColor(Color.FIREBRICK);

		//System.out.println(ImageLoader.font.getLineHeight());
		SB.end();
		//PB.render(SB);
		DetectCollision(SB);
		try {
			if (!BaseBall1.empty() && !Balls.isEmpty()) {
				if (!BaseBall1.elementAt(this.AtwhichBaseBall).GetCollisionCircleMainPlayer().overlaps(Balls.elementAt(this.AtwhichBall).getCollisionCircle())
				) {

					Balls.elementAt(this.AtwhichBall).setCarPistim(false);

				}
			}
		}catch(Exception e){

		}
	
	
		
		
		
		
		CollisionToBands();

		if(!Balls.isEmpty()) {
			int AtWhichBumpBanD = 0;
			while(AtWhichBumpBanD < Balls.size()){
				
				if(!Intersector.overlaps(Balls.elementAt(AtWhichBumpBanD).getCollisionCircle(), band.getBand1()) &&
						!Intersector.overlaps(Balls.elementAt(AtWhichBumpBanD).getCollisionCircle(), band.getBand2())
						&& !Intersector.overlaps(Balls.elementAt(AtWhichBumpBanD).getCollisionCircle(), band.getBand3())){
					Balls.elementAt(AtWhichBumpBanD).setBandaCarptim(false);
				}
				AtWhichBumpBanD++;
			}
			
			if(!BaseBall1.isEmpty()) {
				int AtWhichBaseBallBumpBand = 0;
				
				while(AtWhichBaseBallBumpBand < BaseBall1.size()) {
					
					if(!Intersector.overlaps(BaseBall1.elementAt(AtWhichBaseBallBumpBand).GetCollisionCircleMainPlayer(), band.getBand1())
							&& !Intersector.overlaps(BaseBall1.elementAt(AtWhichBaseBallBumpBand).GetCollisionCircleMainPlayer(), band.getBand2())
							&& !Intersector.overlaps(BaseBall1.elementAt(AtWhichBaseBallBumpBand).GetCollisionCircleMainPlayer(), band.getBand3())) {
						BaseBall1.elementAt(AtWhichBaseBallBumpBand).setBandaCarptim(false);
					}
					AtWhichBaseBallBumpBand++;
				}
				BumpBandMainPlayer();
				
				
			}
			DecetCollisionTOHP();
			GetRandomForNeon();
			while(CountTheTime2 >300) {
				
				
				PB.peek().SetDecideNeonColor(GetRandomForNeon());
				System.out.println("Topun reng" + GetRandomForNeon());
			if(CountTheTime2 >30) {
				CountTheTime2 = 0;
				break;
			}
			
		}
			CountTheTime2++;
			if(!PB.isEmpty()) {
			PB.peek().render(SB);
			}		BumpToNeonForBalls();
			
			
		}
		
	//	ClickingArea.begin(ShapeType.Filled);
		//ClickingArea.rect(0, 0, Gdx.graphics.getWidth(), (float) ( Gdx.graphics.getHeight()/3.75));
		//ClickingArea.setColor(Color.BLACK);
		//ClickingArea.end();
	//	BaseBall.render(SB);
		//	BaseBall1.elementAt(0).render(SB);
		
		//if the stack is not empty, then  it renders the whole balls on the screen 
		if(!BaseBall1.isEmpty()) {

					int CountOfBall = 0;
			//		System.out.println(BaseBall1.size());
					while(CountOfBall < BaseBall1.size()) {
						BaseBall1.elementAt(CountOfBall).render(SB);
						CountOfBall++;
					}
					
		}
		CountTheTime++;
		
	//	System.out.println(Gdx.graphics.getDeltaTime()+ "countthetime" + CountTheTime);
	//	RANDOMX = 0;
		GetRandoq();
		TopHangiDeliktenGelecekRandom();
	while(CountTheTime >120) {
		
		
			if(THDGR<5) {
			PushBilardoBall(Gdx.graphics.getWidth()*(8)/(30) + Gdx.graphics.getWidth()/200,Gdx.graphics.getHeight());
			Balls.peek().SetDecideBallColor(RANDOMX);
			}
			else {
				PushBilardoBall(2*(Gdx.graphics.getWidth()*(8)/(30)) + Gdx.graphics.getWidth()/10 + Gdx.graphics.getWidth()/200,Gdx.graphics.getHeight());
				Balls.peek().SetDecideBallColor(RANDOMX);
			}
				
			
		if(CountTheTime >30) {
			CountTheTime = 0;
			break;
		}
		
	}
			if(!Balls.isEmpty()) {

				int CountOfBilardoBall = 0;
			//	System.out.println(BaseBall1.size());
				while(CountOfBilardoBall < Balls.size()) {
				Balls.elementAt(CountOfBilardoBall).render(SB);
				CountOfBilardoBall++;
				}
			}
			
			if(!Balls.isEmpty()) {
				int NumberOfBall = 0;
				
				while(NumberOfBall < Balls.size()) {
					if(Balls.elementAt(NumberOfBall).getCollisionCircle().x< 0 ||
							Balls.elementAt(NumberOfBall).getCollisionCircle().y< 0 ||
							Balls.elementAt(NumberOfBall).getCollisionCircle().x> Gdx.graphics.getWidth() ||
							Balls.elementAt(NumberOfBall).getCollisionCircle().y> Gdx.graphics.getHeight()) {
					//Efektler çalışamaz
					}
					NumberOfBall++;

				}
				
			}
		
//			SB.begin();
//			SB.draw(ImageLoader.GreenArrow, 0, Gdx.graphics.getHeight() - Gdx.graphics.getHeight()/20, Gdx.graphics.getWidth()*(9)/20, Gdx.graphics.getHeight()/10);
//		//	SB.draw(ImageLoader.BantOfGameReg, this.Band1Xkord, this.Band1Ykord, this.Band1Width, this.Band1Heght);
//			//SB.draw(ImageLoader.BantOfGameReg, this.Band1Xkord, Gdx.graphics.getHeight() - Gdx.graphics.getHeight()/20, this.Band1Xkord, Gdx.graphics.getHeight() - Gdx.graphics.getHeight()/20, 100, 30, 30, 30, 90.0f, true);
//			SB.end();
//			SB.begin();
//			SB.draw(ImageLoader.BantOfGameReg, Gdx.graphics.getWidth()*(9)/20 + (Gdx.graphics.getWidth()/10),Gdx.graphics.getHeight() - (Gdx.graphics.getHeight()/20), Gdx.graphics.getWidth()*(9)/20, Gdx.graphics.getHeight()/10);
//		//	SB.draw(ImageLoader.BantOfGameReg, this.Band2Xkord, this.Band2Ykord, this.Band2Width, this.Band2Height);
//			
//			SB.end();
			CountTheTime3++;

	}
	@Override
	public void update(float delta) {
		DusmeKontrol();
		band.update(delta);
		if(!PB.isEmpty()) {
		PB.peek().update(delta);}
		if(!BaseBall1.isEmpty()) {
			BaseBall1.peek().update(delta);
		}	//	BaseBall1.elementAt(0).update(delta);
		if(!Balls.isEmpty()) {

			int CountOfBilardoBall = 0;
		//	System.out.println(BaseBall1.size());
			while(CountOfBilardoBall < Balls.size()) {
			Balls.elementAt(CountOfBilardoBall).update(delta);
			CountOfBilardoBall++;
			}
		}
		
		
	}

    @Override
    public void destroyElement() {
		ClearStackOfBall();

		PB.peek().destroyElement();

		band = null;
		ArrowCreateArea = null;

    }

    public void PushNewBall(MainPlayer Baseball1) {
		BaseBall1.push(Baseball1);
	}
	
	public static Rectangle getArrowCreateArea() {
		return ArrowCreateArea;
	}
	public static void setArrowCreateArea(Rectangle arrowCreateArea) {
		ArrowCreateArea = arrowCreateArea;
	}
	public MainPlayer GetTopElementOfStackToBuildArrow()
	{
		MainPlayer TopElement = BaseBall1.peek();
		return TopElement;
	}
	
	public int GetStackToCheckEmptyOrNot(){
		
		return BaseBall1.size();
	}
	
	public int GetRandoq() {
		float x = (6*((float) Math.random())) +1;
		 RANDOMX = (int) x;
		return RANDOMX;
	}
	public int TopHangiDeliktenGelecekRandom() {
		float y = ((float)(Math.random())) * 10;
		THDGR = (int) y;
		return THDGR;
		
	}
	public void PushBilardoBall(int xkordBalls, int yKordBalls) {
		Balls.push(new BallsForLV2(xkordBalls, yKordBalls, Gdx.graphics.getWidth()/10, Gdx.graphics.getWidth()/10));
	}
	
	public int GetRandomForNeon() {
		float x = 10*((float) Math.random());
		 RANDOMForNeon = (int) x;
		return RANDOMForNeon;
	}
	
	
	
	public void DetectCollision(SpriteBatch SB) {
		int AtWhichBaseBall = 0;
		int AtWhichBall = 0;
		boolean DidIt = false;
		
		
		if(!BaseBall1.isEmpty() && !Balls.isEmpty()) {
			int CountOfBaseBall = 0;
		
			while(CountOfBaseBall < BaseBall1.size()) {
				
				int CountOfBall =0;
				
				while(CountOfBall <Balls.size()) {
					
					if(BaseBall1.elementAt(CountOfBaseBall).GetCollisionCircleMainPlayer().overlaps(Balls.elementAt(CountOfBall).getCollisionCircle())) {
						System.out.println("Çarptın");
						System.out.println("Çarptığın nokta" + BaseBall1.elementAt(CountOfBaseBall).GetCollisionCircleMainPlayer().y);
					AtWhichBall = CountOfBall;
					AtWhichBaseBall = CountOfBaseBall;
					this.AtwhichBall = AtWhichBall;
					this.AtwhichBaseBall = AtWhichBaseBall;
				
					DidIt = true;
						break;
					}
					
					CountOfBall++;
				}
				CountOfBaseBall++;
			}
			//System.out.println("whilenin dışına çıktı");

		}
		
	
	if(DidIt && !Balls.elementAt(AtWhichBall).isCarPistim() && 
			BaseBall1.elementAt(AtWhichBaseBall).isTouchUp()) {
		
		
		
		
		Balls.elementAt(AtWhichBall).setCarPistim(true);
		
		float CollisionX = (Balls.elementAt(AtWhichBall).getCollisionCircle().x +
				BaseBall1.elementAt(AtWhichBaseBall).GetCollisionCircleMainPlayer().x)/2;
		
		float CollisionY = (Balls.elementAt(AtWhichBall).getCollisionCircle().y +
				BaseBall1.elementAt(AtWhichBaseBall).GetCollisionCircleMainPlayer().y)/2;
		
		float BilardoX = Balls.elementAt(AtWhichBall).getCollisionCircle().x;
		float BilardoY = Balls.elementAt(AtWhichBall).getCollisionCircle().y;
		
	//	System.out.println("SpeedOfMainPlayerAtCollision" + BaseBall1.elementAt(AtWhichBaseBall).getSpeedX());
		float SpeedOfMainPlayerInCollisionAsX = BaseBall1.elementAt(AtWhichBaseBall).getSpeedX();
		float SpeedOfMainPlayerInCollisionAsY = BaseBall1.elementAt(AtWhichBaseBall).getSpeedY();
		
		//float Rate = Math.abs((CollisionY-BilardoY) / (CollisionX - BilardoX));
		//System.out.println("Rate" +Rate);
		
		float UzaklıkX = CollisionX -BilardoX;
		float UzaklıkY = CollisionY -BilardoY;
		this.CollisionX = CollisionX;
		this.CollisionY = CollisionY;
		float Rate = Math.abs((CollisionY-BilardoY) / Balls.elementAt(AtWhichBall).getR());
		
		Balls.elementAt(AtWhichBall).setBallsSpeedX(SpeedOfMainPlayerInCollisionAsX*(1-Rate));
		Balls.elementAt(AtWhichBall).setBallsSpeedY(SpeedOfMainPlayerInCollisionAsY*(Rate));
		BaseBall1.elementAt(AtWhichBaseBall).setSpeedX(SpeedOfMainPlayerInCollisionAsX*(1/2)*(-1));
		BaseBall1.elementAt(AtWhichBaseBall).setSpeedY(SpeedOfMainPlayerInCollisionAsY*(-1)*(1/2));
		
		
		
		
		Balls.elementAt(AtWhichBall).setPushParticleEffect(true);
	}
		
	
	}
	
	
	
	public void CollisionToBands() {
		int BallsAmount = 0;
		if(!Balls.isEmpty()) {
			
			while(BallsAmount < Balls.size() && !Balls.elementAt(BallsAmount).isBandaCarptim()) {
					if(!Balls.elementAt(BallsAmount).isBandaCarptim() &&  Intersector.overlaps(Balls.elementAt(BallsAmount).getCollisionCircle(), band.getBand1()) ||
							Intersector.overlaps(Balls.elementAt(BallsAmount).getCollisionCircle(),band.getBand2())
							 || Intersector.overlaps(Balls.elementAt(BallsAmount).getCollisionCircle(), band.getBand3())){
							Balls.elementAt(BallsAmount).setBallsSpeedY(Balls.elementAt(BallsAmount).getBallsSpeedY()*(-1));
							Balls.elementAt(BallsAmount).setBandaCarptim(true);
					}
					this.AtWhichBallBumpBand = BallsAmount;

					BallsAmount++;
			}
		}

		
	
			
}
	
	public void DusmeKontrol() {
		int DusmeKontrolSayisi = 0;
		while(DusmeKontrolSayisi <Balls.size()) {
			if(!Balls.elementAt(DusmeKontrolSayisi).isNeonaCarptim() && Balls.elementAt(DusmeKontrolSayisi).getY()<0
					&& !Balls.elementAt(DusmeKontrolSayisi).isDusmeIslemi())
					{
						
						Balls.elementAt(DusmeKontrolSayisi).setDusmeIslemi(true);
						KalanCan--;
					}
			DusmeKontrolSayisi++;
		}
	}
	
	public void BumpBandMainPlayer() {
		
		
		int MainPlayerAmount = 0;
		
		while(MainPlayerAmount < BaseBall1.size()) {
			if(!BaseBall1.elementAt(MainPlayerAmount).isBandaCarptim() &&  ( Intersector.overlaps(BaseBall1.elementAt(MainPlayerAmount).GetCollisionCircleMainPlayer(), band.getBand1())
				|| Intersector.overlaps(BaseBall1.elementAt(MainPlayerAmount).GetCollisionCircleMainPlayer(), band.getBand2()))
					|| Intersector.overlaps(BaseBall1.elementAt(MainPlayerAmount).GetCollisionCircleMainPlayer(), band.getBand3())){
				BaseBall1.elementAt(MainPlayerAmount).setSpeedY(BaseBall1.elementAt(MainPlayerAmount).getSpeedY()*(-1));
				
			}
			MainPlayerAmount++;
			
		}
	}
		
			
		public void PushNeon(int RandomNeon) {
			PB.push(new PointBandsForLV2(RandomNeon));
			
		}
	
	
		public void BumpToNeonForBalls() {
			int BallNumbers = 0;
			
			while(BallNumbers < Balls.size() && !Balls.isEmpty()) {
				if(Intersector.overlaps(Balls.elementAt(BallNumbers).getCollisionCircle(), PB.peek().getNeon1Rect())) {
					if(Balls.elementAt(BallNumbers).getRenk() == 1
							&& !Balls.elementAt(BallNumbers).isNeonaCarptim()){
						//puan++
						if(X2Ball) {
						Puan = Puan + 2;
						}
						else {
						Puan++;
						}
						Balls.elementAt(BallNumbers).setNeonaCarptim(true);
					}
					else if(!Balls.elementAt(BallNumbers).isNeonaCarptim()){
						//can --
						System.out.println("can kaybettin");
						KalanCan--;
						Balls.elementAt(BallNumbers).setNeonaCarptim(true);

					}
				}
				
				else if(Intersector.overlaps(Balls.elementAt(BallNumbers).getCollisionCircle(), PB.peek().getNeon2Rect())) {
					if(Balls.elementAt(BallNumbers).getRenk() == 2
							&& !Balls.elementAt(BallNumbers).isNeonaCarptim()){
						//puan++
						if(X2Ball) {
							Puan = Puan + 2;
							}
							else {
							Puan++;
							}
						System.out.println("puan aldın");
					Balls.elementAt(BallNumbers).setNeonaCarptim(true);
					}
				
					else if(!Balls.elementAt(BallNumbers).isNeonaCarptim()){
						//can --
						System.out.println("can kaybettin");
						KalanCan--;
						Balls.elementAt(BallNumbers).setNeonaCarptim(true);
//
//							
				}
//					
				}
				
				
				
			
				
					
//					
				
//				else if(!Intersector.overlaps(Balls.elementAt(BallNumbers).getCollisionCircle(),PB.peek().getNeonYellowRect() )) {
//					if(Balls.elementAt(BallNumbers).getRenk() == 3 && !Balls.elementAt(BallNumbers).isNeonaCarptim()) {
//						Puan++;
//						Balls.elementAt(BallNumbers).setNeonaCarptim(true);
//					}
//					else if(!Balls.elementAt(BallNumbers).isNeonaCarptim()) {
//						KalanCan--;
//						Balls.elementAt(BallNumbers).setNeonaCarptim(true);
//					}
//				}
//				
//				
//				else if(!Intersector.overlaps(Balls.elementAt(BallNumbers).getCollisionCircle(), PB.peek().getNeonGreenRect())) {
//					
//					if(Balls.elementAt(BallNumbers).getRenk() == 4 && !Balls.elementAt(BallNumbers).isBandaCarptim()) {
//						Puan++;
//						Balls.elementAt(BallNumbers).setNeonaCarptim(true);
//					}
//					
//					else if(!Balls.elementAt(BallNumbers).isNeonaCarptim()) {
//						KalanCan--;
//						Balls.elementAt(BallNumbers).setNeonaCarptim(true);
//					}
//					
//					
//				}
				
			BallNumbers++;
			}
			
			
		
			
		}
		public int getPuan() {
			return Puan;
		}
		public void setPuan(int puan) {
			Puan = puan;
		}
		public int getKalanCan() {
			return KalanCan;
		}
		public void setKalanCan(int kalanCan) {
			KalanCan = kalanCan;
		}
	
	
	
//	public void Collision() {
//		
//		int AtWhichBaseBall = 0;
//		int AtWhichBall = 0;
//		
//		
//		if(!BaseBall1.isEmpty() && !Balls.isEmpty()) {
//			boolean DidIt = false;
//			
//			int CountOfBalls = 0;
//			while(CountOfBalls < Balls.size() ) {
//				int CountOfMainPlayer = 0;
//			
//				while(CountOfMainPlayer < BaseBall1.size()){
//					boolean x = 
//						
//					if(x) {
//						x = false;
//						AtWhichBaseBall = CountOfMainPlayer;
//						AtWhichBall = CountOfBalls;
//						
//						System.out.println("ÇARPTINN...");
//						System.out.println("Çarpan top" + CountOfMainPlayer);
//						DidIt = true;
//					break;
//					}
//					if(DidIt) {
//						break;
//					}
//						
//					CountOfMainPlayer++;}
//			
//				if(DidIt) {
//					break;
//				}
//			CountOfBalls++;
//			}
//			
//	
//			if(DidIt) {
//				
//				//Detect the collision point
//				DidIt = false;
//		
//				
//				
//				
//				
//				float CollisionX = (Balls.elementAt(AtWhichBall).getCollisionCircle().x +
//						BaseBall1.elementAt(AtWhichBaseBall).GetCollisionCircleMainPlayer().x)/2;
//				
//				float CollisionY = (Balls.elementAt(AtWhichBall).getCollisionCircle().y +
//						BaseBall1.elementAt(AtWhichBaseBall).GetCollisionCircleMainPlayer().y)/2;
//				
//				float BilardoX = Balls.elementAt(AtWhichBall).getCollisionCircle().x;
//				float BilardoY = Balls.elementAt(AtWhichBall).getCollisionCircle().y;
//				
//			//	System.out.println("SpeedOfMainPlayerAtCollision" + BaseBall1.elementAt(AtWhichBaseBall).getSpeedX());
//				float SpeedOfMainPlayerInCollisionAsX = BaseBall1.elementAt(AtWhichBaseBall).getSpeedX();
//				float SpeedOfMainPlayerInCollisionAsY = BaseBall1.elementAt(AtWhichBaseBall).getSpeedY();
//				
//				//float Rate = Math.abs((CollisionY-BilardoY) / (CollisionX - BilardoX));
//				//System.out.println("Rate" +Rate);
//				
//				float UzaklıkX = CollisionX -BilardoX;
//				float UzaklıkY = CollisionY -BilardoY;
//				float Rate = Math.abs((CollisionY-BilardoY) / Balls.elementAt(AtWhichBall).getR());
//				
//				Balls.elementAt(AtWhichBall).setBallsSpeedX(SpeedOfMainPlayerInCollisionAsX);
//				Balls.elementAt(AtWhichBall).setBallsSpeedY(SpeedOfMainPlayerInCollisionAsY);
//				BaseBall1.elementAt(AtWhichBaseBall).setSpeedX(0);
//				BaseBall1.elementAt(AtWhichBaseBall).setSpeedY(0);
//				
//				
//				
//				
//				
//				
//				
//				
//				
//				
//				//oran kadar ye dağılır
////				if(UzaklıkX < 0 && UzaklıkY >0) {
////					//sol üstten
////					if(SpeedOfMainPlayerInCollisionAsY>0) {
////						
////						Balls.elementAt(AtWhichBall).setBallsSpeedX(SpeedOfMainPlayerInCollisionAsX*Rate);
////						Balls.elementAt(AtWhichBall).setBallsSpeedY(SpeedOfMainPlayerInCollisionAsX*(1-Rate)*(-1));
////						BaseBall1.elementAt(AtWhichBaseBall).setSpeedX(0);
////					}
////					
////					else if(SpeedOfMainPlayerInCollisionAsY<0) {
////						
////						Balls.elementAt(AtWhichBall).setBallsSpeedX(SpeedOfMainPlayerInCollisionAsY*Rate);
////						Balls.elementAt(AtWhichBall).setBallsSpeedY(SpeedOfMainPlayerInCollisionAsY*(1-Rate)*(-1));
////						BaseBall1.elementAt(AtWhichBaseBall).setSpeedY(0);
////					}
////					
////				}
////				
////				
////				//Sağ üstten
////				else if(UzaklıkX > 0 && UzaklıkY >0) {
////					
////					if(SpeedOfMainPlayerInCollisionAsY>0) {
////						//y hesaba katılmaz
////						
////						Balls.elementAt(AtWhichBall).setBallsSpeedX(SpeedOfMainPlayerInCollisionAsX*Rate);
////						Balls.elementAt(AtWhichBall).setBallsSpeedY(SpeedOfMainPlayerInCollisionAsX*(1-Rate)*(-1));
////						BaseBall1.elementAt(AtWhichBaseBall).setSpeedX(0);
////					}
////					else if(SpeedOfMainPlayerInCollisionAsY<0) {
////						//x hesaba katılmaz
////						
////						Balls.elementAt(AtWhichBall).setBallsSpeedX(SpeedOfMainPlayerInCollisionAsY*Rate);
////						Balls.elementAt(AtWhichBall).setBallsSpeedY(SpeedOfMainPlayerInCollisionAsY*(1-Rate)*(-1));
////						BaseBall1.elementAt(AtWhichBaseBall).setSpeedY(0);
////						
////					}
////					
////				}
////				
////				//sağ alttan
////				else if(UzaklıkX>0 && UzaklıkY <0) {
////					
////					if(SpeedOfMainPlayerInCollisionAsX>0) {
////						//x hesaba katılmaz
////					
////					Balls.elementAt(AtWhichBall).setBallsSpeedX(SpeedOfMainPlayerInCollisionAsY*Rate);
////					Balls.elementAt(AtWhichBall).setBallsSpeedY(SpeedOfMainPlayerInCollisionAsY*(1-Rate));
////					BaseBall1.elementAt(AtWhichBaseBall).setSpeedY(0);
////					}
////					else if(SpeedOfMainPlayerInCollisionAsY<0) {
////						//yhesaba katılmaz
////						Balls.elementAt(AtWhichBall).setBallsSpeedY(SpeedOfMainPlayerInCollisionAsX*Rate);
////						Balls.elementAt(AtWhichBall).setBallsSpeedX(SpeedOfMainPlayerInCollisionAsX*(1-Rate));
////						BaseBall1.elementAt(AtWhichBaseBall).setSpeedX(0);
////					}
////				}
////					
////				
//				
//				
//				
////				float NewSpeedXOfBall = BaseBall1.elementAt(AtWhichBaseBall).getSpeedX();
////						
////
////				float NewSpeedYofBall = BaseBall1.elementAt(AtWhichBaseBall).getSpeedY();
////				
////						
////					Balls.elementAt(AtWhichBall).setBallsSpeedX(NewSpeedXOfBall);
////					Balls.elementAt(AtWhichBall).setBallsSpeedY(NewSpeedYofBall);
////				
////				
//				
//				
//				
//				
//				
////			if((Balls.elementAt(AtWhichBall).getCollisionCircle().x - 
////					BaseBall1.elementAt(AtWhichBaseBall).GetCollisionCircleMainPlayer().x)<0
////					
////					&& (BaseBall1.elementAt(AtWhichBaseBall).getFirstXCordOfMainPlayer()-
////					BaseBall1.elementAt(AtWhichBaseBall).getxKord()) < 0) {
////				//X's destination does not be calculated
////				
////				//////Calculation of degree
////				
////			float BallSpeedY= Balls.elementAt(AtWhichBall).getBallsSpeedY();
////				
////				BallSpeedY = Rate;
////				Balls.elementAt(AtWhichBall).setBallsSpeedY(BallSpeedY);				
////				
////				
////			}
////			
////			
////			if((Balls.elementAt(AtWhichBall).getCollisionCircle().x - 
////					BaseBall1.elementAt(AtWhichBaseBall).GetCollisionCircleMainPlayer().x)>0
////					
////					&& (BaseBall1.elementAt(AtWhichBaseBall).getFirstXCordOfMainPlayer()-
////					BaseBall1.elementAt(AtWhichBaseBall).GetCollisionCircleMainPlayer().x) > 0) {
////				
////				//X's destination does not be calculated,sol üstten
////				
////				//ball x - basex = 0 and ball y - base y >0  y  enerji direk
////				
////				
////				
////				
////				
////			}
////			
////			if((BaseBall1.elementAt(AtWhichBaseBall).GetCollisionCircleMainPlayer().y -
////					Balls.elementAt(AtWhichBall).getCollisionCircle().y) > 0 
////					&& (BaseBall1.elementAt(AtWhichBaseBall).GetCollisionCircleMainPlayer().y -
////					BaseBall1.elementAt(AtWhichBaseBall).getFirstYCordOfMainPlayer()) > 0) {
////				
////			
////			//Y's Destination does not be calculated
////			
////			}
////			
////			if((BaseBall1.elementAt(AtWhichBaseBall).GetCollisionCircleMainPlayer().y -
////					Balls.elementAt(AtWhichBall).getCollisionCircle().y) < 0 
////					&& (BaseBall1.elementAt(AtWhichBaseBall).GetCollisionCircleMainPlayer().y -
////					BaseBall1.elementAt(AtWhichBaseBall).getFirstYCordOfMainPlayer()) < 0) {
////				//Y's Destination does not be calculated
////		
////			
////			
////			}
////			
////			if((Balls.elementAt(AtWhichBall).getCollisionCircle().x -
////					BaseBall1.elementAt(AtWhichBaseBall).GetCollisionCircleMainPlayer().x) == 0) {
////				
////				float SetBallSpeedYOfBalls = BaseBall1.elementAt(AtWhichBaseBall).getSpeedY()*(-1);
////				Balls.elementAt(AtWhichBaseBall).setBallsSpeedY(SetBallSpeedYOfBalls);
////			}
////			
////			
//			
//			
//			
//			}
//			
//			
//			
//			
//			
//		}
//		
//		
//	}
//	
//	
	
	
		public void PushCanTopu() {
			System.out.println("Can tpu");
			int HP =  (int) ((20 * Math.random()) + 1);
			boolean Mantar = true;
			if( HP < 6 &&KalanCan <= 1) {
				
				HPBall.push(new HPBall(Gdx.graphics.getWidth()*(8)/(30) + Gdx.graphics.getWidth()/200,Gdx.graphics.getHeight()));
				HPBall.peek().setType((byte) 1);
				Mantar = false;
			}
			else if(HP > 17) {
				
				HPBall.push(new HPBall(Gdx.graphics.getWidth()*(8)/(30) + Gdx.graphics.getWidth()/200,Gdx.graphics.getHeight()));
				HPBall.peek().setType((byte)2); 
			}
			else if(HP > 10 && HP < 13) {
				
				HPBall.push(new HPBall(Gdx.graphics.getWidth()*(8)/(30) + Gdx.graphics.getWidth()/200,Gdx.graphics.getHeight()));
				HPBall.peek().setType((byte)3); 

			}
		}
			
			
//			else if(HP > 7 && HP < 10) {
//				HPBall.push(new HPBall(Gdx.graphics.getWidth()*(8)/(30) + Gdx.graphics.getWidth()/200,Gdx.graphics.getHeight()));
//				HPBall.peek().setType((byte)3); 
//			}

			public void DecetCollisionTOHP()
			{try {
				if(!BaseBall1.isEmpty() && !HPBall.isEmpty()) {
					int CountOfBaseBall = 0;
				
					while(CountOfBaseBall < BaseBall1.size()) {
						
						int CountOfBall =0;
						
						while(CountOfBall <HPBall.size()) {
							
							if(BaseBall1.elementAt(CountOfBaseBall).GetCollisionCircleMainPlayer().overlaps(HPBall.elementAt(CountOfBall).getBallsCircle())
									&& !HPBall.elementAt(CountOfBall).isCarPistim()) {
								if( HPBall.elementAt(CountOfBall).getType() == 1) {
								KalanCan = 2;

								HPBall.elementAt(CountOfBall).setCarPistim(true);
								HPBall.remove(CountOfBall);
								break;
								}
								else if(HPBall.elementAt(CountOfBall).getType() == 2) {
									
									KalanCan =0;
									HPBall.elementAt(CountOfBall).setCarPistim(true);
									HPBall.remove(CountOfBall);
									break;
								}
							else if(HPBall.elementAt(CountOfBall).getType() == 3) {
								
									X2Ball = true;
									HPBall.elementAt(CountOfBall).setCarPistim(true);
									HPBall.remove(CountOfBall);
									break;
								}
							}
							
							CountOfBall++;
						}
						CountOfBaseBall++;
					}
			}
			}catch(Exception e) {
				System.out.println("başka bir hata meydana geldi");
			}
		}



	public void ClearStackOfBall() {
		int ClearBalls = 0;

		while(ClearBalls < BaseBall1.size() && BaseBall1.size() > 5){
			if(BaseBall1.elementAt(ClearBalls).getyKord() < -100) {
				BaseBall1.elementAt(ClearBalls).destroyElement();
				BaseBall1.remove(ClearBalls);
			}
			ClearBalls++;

		}
//i
		int ClearBilardo = 0;
		while(ClearBilardo < Balls.size() && Balls.size() > 5){
			if(Balls.elementAt(ClearBilardo).getY() < -100) {
				Balls.elementAt(ClearBilardo).destroyElement();
				Balls.remove(ClearBilardo);
			}
			ClearBilardo++;

		}
//		if(PB.size() > 2) {
//			PB.remove(0);
//		}




	}
}
