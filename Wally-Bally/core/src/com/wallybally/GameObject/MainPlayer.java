package com.wallybally.GameObject;

import java.awt.LinearGradientPaint;

import javax.swing.event.MouseInputListener;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.wallybally.game.*;

import com.wallybally.ImageLoader.*;
import com.wallybally.states.*;
import com.wallybally.InputHandler.*;

public class MainPlayer implements GameOBject {
	//playerin koordinatları
	
	private  float xKord;
	private  float yKord;
	private   float Width;
	private  float Height;
	private  float SpeedX;
	private  float SpeedY;
	private float FirstXCordOfMainPlayer;
	private float FirstYCordOfMainPlayer;

	
	
	private  float Acceleration;
	private  Circle ShadowMainPlayer;
	private  ShapeRenderer LineArrow;
	private  float LineArrowX;
	private  float LineArrowY;
	private  float LineArrowEndPointX;
	private  float LineArrowEndPointY;
	private float BaseBallAcc;
	//////
	private boolean TouchUp;
	////
	private boolean CreatedBall=false;
	public boolean TopIsSent = false;
	public static float UzaklıkX;
	public static float UzaklıkY;
	public  boolean isTouchONBallArea = false;
	public boolean TouchOnBallAreaIs = true;
	
	private boolean BandaCarptim;
	
	public MainPlayer(float xKord, float yKord, float Width, float Height) {
	//System.out.println("bu yeni top");
	ShadowMainPlayer = new Circle(xKord+(Width/2),yKord +(Height/2) , 19);
	UzaklıkX = 0;
	UzaklıkY = 0;
	this.xKord = xKord;
	this.yKord = yKord;
	this.Width = Width;
	this.Height = Height;
	Acceleration = 0.01f;
	BaseBallAcc = 0.01f;
	TouchUp = false;
	/////
	FirstXCordOfMainPlayer = ShadowMainPlayer.x;
	FirstYCordOfMainPlayer = ShadowMainPlayer.y;
	/////
	

	LineArrowX = xKord+(Width/2);
	LineArrowY =yKord +(Height/2);
	LineArrowEndPointX = xKord+(Width/2);
	LineArrowEndPointY = yKord +(Height/2);
	
	
	BandaCarptim = false;
///////////////////////////////////	
	
		//////////////////////////////////////////////////
	
		
	//	System.out.println("LineArrowEndPointX" + LineArrowEndPointX);
		//System.out.println(LineArrowEndPointY);
	
		
	
	}
	
	public float getSpeedX() {
		return SpeedX;
	}

	public void setSpeedX(float speedX) {
		SpeedX = speedX;
	}

	public float getSpeedY() {
		return SpeedY;
	}

	public void setSpeedY(float speedY) {
		SpeedY = speedY;
	}

	@Override
	public void render(SpriteBatch SB) {
	
//		System.out.println(GetLineArrowEndPointX()+ "" +GetLineArrowEndPointY());
		///MainPlayerin ShapeRenderer'ı
	
		if(isTouchONBallArea && TouchOnBallAreaIs) {
			CreateArrow();
		//	System.out.println("CReateArrow");
			SpeedX = (float) ((GetLineArrowEndPointX()- LineArrowX)*0.100);
			
			SpeedY = (float)((GetLineArrowEndPointY() - LineArrowY)*0.100);
			
	}
		Acceleration = (float) (UzaklıkX*0.1);
		if(!isTouchONBallArea && !CreatedBall) {
		//	SpeedX = (float) ((GetLineArrowEndPointX()- LineArrowX)*0.05);
			getSpeedX();
			xKord += SpeedX;
			//SpeedY = (float)((GetLineArrowEndPointY() - LineArrowY)*0.05);
			getSpeedY();
			if(TouchUp) {
			 BaseBallAcc += 0.005f;
			}
			else BaseBallAcc = 0;
			SpeedY = SpeedY - BaseBallAcc;
			yKord += SpeedY;
		//	System.out.println("SpeedXOfMain" + SpeedX);
			//;
		
			//System.out.println("SpeedYOfMain" + SpeedY);
		}
	
		SB.begin();
		SB.draw(ImageLoader.text,xKord,yKord,Width ,Height );
		SB.end();
		
			
		ShadowMainPlayer.x = xKord+(Width/2);
		ShadowMainPlayer.y = yKord +(Height/2);
		ShadowMainPlayer.radius = 19;
	
		
		
	}

	public boolean isTouchUp() {
		return TouchUp;
	}

	public void setTouchUp(boolean touchUp) {
		TouchUp = touchUp;
	}

	@Override
	public void update(float delta) {
		
	//	System.out.println("Widht" + Width);
		
	}

	@Override
	public void destroyElement() {
ShadowMainPlayer = null;
LineArrow.dispose();
	}

	public Circle GetMainPlayRender() {
		return ShadowMainPlayer;
	}
	
	public float GetLineArrowEndPointX() {
		return LineArrowEndPointX;
	}
	
	public float GetLineArrowEndPointY() {
		return LineArrowEndPointY;
	}
	
	public void SetLineArrowEndPointX(float LineArrowEndPointX) {
		this.LineArrowEndPointX = LineArrowEndPointX;
	}
	
	public void SetLineArrowEndPointY(float LineArrowEndPointY) {
		this.LineArrowEndPointY = LineArrowEndPointY;
	}
	
	public float GetLineArrowStartPointX() {
		return LineArrowX;
	}
	
	public float GetLineArrowStartPointY() {
		return LineArrowY;
	}

	
	public void CreateArrow() {
		
		if(TopIsSent == false) {
		LineArrow = new ShapeRenderer();
	//	System.out.println("burada");
		LineArrowEndPointX = 2*(GetLineArrowStartPointX()- UzaklıkX);
		LineArrowEndPointY =2* (GetLineArrowStartPointY() - UzaklıkY);
//		System.out.println("Buradaki uzaklıkX" + UzaklıkX);
	//	System.out.println("Buradaki uzaklıkY" + UzaklıkY);
		LineArrow.begin(ShapeType.Line);
		
		LineArrow.setColor(Color.RED);
	
		LineArrow.line(LineArrowX,LineArrowY, LineArrowX - (2* UzaklıkX), LineArrowY -(2* UzaklıkY));
		LineArrow.end();
	
		
		SetLineArrowEndPointX(LineArrowX - (2* UzaklıkX));
		SetLineArrowEndPointY(LineArrowY -(2* UzaklıkY));
		
		
	//	System.out.println("LineArrowEndPointX" + LineArrowEndPointX);
		//System.out.println(LineArrowEndPointY);
	
		}
	//	TouchOnBallAreaIs = false;
	}
	
	public void PushTheBall(float UzaklıkX, float UzaklıkY) {
		xKord -= UzaklıkX*0.1;
		yKord += UzaklıkX*0.1;
	//	System.out.println("push ediliyor");
	}
	public void New() {
		new MainPlayer(90, 100, 50, 50);
	}
	
	
	public void SetCreatedBall(boolean CreatedBall) {
		 this.CreatedBall = CreatedBall;
	}
	
	public float GetWidthOfBaseBall() {
		return Width;
	}
	public float GetHeightOfBaseBall() {
		return Height;
	}
	
	public Circle GetCollisionCircleMainPlayer() {
		return ShadowMainPlayer;
	}
	
	
	
	public float getFirstXCordOfMainPlayer() {
		return FirstXCordOfMainPlayer;
	}
	public void setFirstXCordOfMainPlayer(float firstXCordOfMainPlayer) {
		FirstXCordOfMainPlayer = firstXCordOfMainPlayer;
	}
	public float getFirstYCordOfMainPlayer() {
		return FirstYCordOfMainPlayer;
	}
	public void setFirstYCordOfMainPlayer(float firstYCordOfMainPlayer) {
		FirstYCordOfMainPlayer = firstYCordOfMainPlayer;
	}

	public float getxKord() {
		return xKord;
	}

	public void setxKord(float xKord) {
		this.xKord = xKord;
	}

	public float getyKord() {
		return yKord;
	}

	public void setyKord(float yKord) {
		this.yKord = yKord;
	}

	public boolean isBandaCarptim() {
		return BandaCarptim;
	}

	public void setBandaCarptim(boolean bandaCarptim) {
		BandaCarptim = bandaCarptim;
	}

}

