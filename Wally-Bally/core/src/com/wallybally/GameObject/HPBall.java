package com.wallybally.GameObject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.wallybally.ImageLoader.ImageLoader;

public class HPBall implements GameOBject {
	private float XKordBalls;
	private float YKordBalls;
	private  float BallsSpeed;
	private  float BallsAc;
	private Circle BallsCircle;

	private float x;
	private float y;
	private float r;

	private float WidthBalls;
	private float HeightBalls;

	private boolean CarPistim;
	

	
	private byte Type;// if 1 = healtg , if 2 bomb if 3 x2 if 4 joker 
	

	public byte getType() {
		return Type;
	}
	public void setType(byte type) {
		Type = type;
	}
	public boolean isCarPistim() {
		return CarPistim;
	}
	public void setCarPistim(boolean carPistim) {
		CarPistim = carPistim;
	}
	public HPBall(int XKordBall, int YkordBall) {
		WidthBalls = Gdx.graphics.getWidth()/10;
		HeightBalls = Gdx.graphics.getWidth()/10;
		this.XKordBalls = XKordBall;
		this.YKordBalls = YkordBall;
		x = XKordBalls+(WidthBalls/2);
		y = YKordBalls+ (HeightBalls/2);
		BallsCircle = new Circle(x, y, r);
		CarPistim = false;
		Type = 0;
	}
	public Circle getBallsCircle() {
		return BallsCircle;
	}
	public void setBallsCircle(Circle ballsCircle) {
		BallsCircle = ballsCircle;
	}
	@Override
	public void render(SpriteBatch SB) {
	
		if(Type == 1) {
			Health(SB);
		}
		
		else if(Type == 2) {
			Bomb(SB);
		}
		
		else if(Type == 3) {
			x2Ball(SB);
		}
		x = XKordBalls +(WidthBalls/2);
		y = YKordBalls+ (HeightBalls/2);
		BallsCircle.x = x;
		BallsCircle.y = y;
		
		
		BallsAc -= 0.05f;
		YKordBalls += BallsAc;
		
	}
	
	
	public void Health(SpriteBatch SB) {//if type = 1
		SB.begin();
		
		SB.draw(ImageLoader.HPBallReg, XKordBalls, YKordBalls, Gdx.graphics.getWidth()/10, Gdx.graphics.getWidth()/10);
		SB.end();
		
	}
	
	public void Bomb(SpriteBatch SB) {
		SB.begin();
		
		SB.draw(ImageLoader.Bomb, XKordBalls, YKordBalls, Gdx.graphics.getWidth()/10, Gdx.graphics.getWidth()/10);
		SB.end();
	}
	
	public void Joker(SpriteBatch SB) {
SB.begin();
		
		SB.draw(ImageLoader.Joker, XKordBalls, YKordBalls, Gdx.graphics.getWidth()/10, Gdx.graphics.getWidth()/10);
		SB.end();
		
	}
	public void x2Ball(SpriteBatch SB) {
		
		SB.begin();
		SB.draw(ImageLoader.x2Ball, XKordBalls, YKordBalls, Gdx.graphics.getWidth()/10, Gdx.graphics.getWidth()/10);

		SB.end();
	}
	@Override
	public void update(float delta) {

		
	}

	@Override
	public void destroyElement() {

	}

}
