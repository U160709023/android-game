package com.wallybally.GameObject;

import javax.print.attribute.standard.SheetCollate;

// siyah top collisionda can götürür
//beyaz top vurulduğunda bütün renkler eşit olur seri seri top gelir
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.wallybally.ImageLoader.*;
import com.wallybally.GameObject.*;


public class PointBandsForLV3 implements GameOBject{

private  int DecideRandomNeon;
private Rectangle NeonRedRect;//for red
private Rectangle NeonBlueRect;//for blue
private Rectangle NeonYellowRect;//for yellow
private Rectangle NeonGreenRect;//for green
private byte RenkOfNeon1; //1 = red, 2= blue, 3= yellow, 4 = green(Sol üst)
private byte RenkOfNeon2; // Sağ(üst)
//
//private static boolean JokerBall;
//private static  int JokerCount;
private  ParticleEffect RedNeonEffect;
private  ParticleEffect BlueNeonEffect;
private  ParticleEffect YellowNeonEffect;
private  ParticleEffect GreenNeonEffect;
private int Count;
//private ParticleEffect RedNeonEffectForJoker;
//private ParticleEffect RedNeonEffectForJoker1;
//private ParticleEffect RedNeonEffectForJoker2;
//private ParticleEffect RedNeonEffectForJoker3;


//private ShapeRenderer SR;
public PointBandsForLV3(int DecideRandomNeon) {
	Count = 5400;
	//SR = new ShapeRenderer();
	//JokerBall = false;
	RedNeonEffect = new ParticleEffect();
	RedNeonEffect.load(Gdx.files.internal("redneon1.p"), Gdx.files.internal(""));
	RedNeonEffect.start();

	YellowNeonEffect = new ParticleEffect();
	YellowNeonEffect.load(Gdx.files.internal("sarilarinsulo.p"), Gdx.files.internal(""));
	YellowNeonEffect.start();

	BlueNeonEffect = new ParticleEffect();
	BlueNeonEffect.load(Gdx.files.internal("blue3.p"), Gdx.files.internal(""));
	BlueNeonEffect.start();

	GreenNeonEffect = new ParticleEffect();
	GreenNeonEffect.load(Gdx.files.internal("greenneon.p"), Gdx.files.internal(""));
	GreenNeonEffect.start();

	//JokerCount = 0;
//		RedNeonEffectForJoker = new ParticleEffect();
//		  RedNeonEffectForJoker.load(Gdx.files.internal("redneon1.p"), Gdx.files.internal(""));
//		  RedNeonEffectForJoker.start();
//		  
//		  RedNeonEffectForJoker1 = new ParticleEffect();
//		  RedNeonEffectForJoker1.load(Gdx.files.internal("redneon1.p"), Gdx.files.internal(""));
//		  RedNeonEffectForJoker1.start();
//		  
//		  RedNeonEffectForJoker2 = new ParticleEffect();
//		  RedNeonEffectForJoker2.load(Gdx.files.internal("redneon1.p"), Gdx.files.internal(""));
//		  RedNeonEffectForJoker2.start();
//		  
//		  RedNeonEffectForJoker3 = new ParticleEffect();
//		  RedNeonEffectForJoker3.load(Gdx.files.internal("redneon1.p"), Gdx.files.internal(""));
//		  RedNeonEffectForJoker3.start();

	BlueNeonEffect.scaleEffect(Gdx.graphics.getWidth() / 10, Gdx.graphics.getHeight() / 2, 0.001f);
	GreenNeonEffect.scaleEffect(Gdx.graphics.getWidth() / 10, Gdx.graphics.getHeight() / 2, 0.001f);
	RedNeonEffect.scaleEffect(Gdx.graphics.getWidth() / 10, Gdx.graphics.getHeight() / 2, 0.001f);
	YellowNeonEffect.scaleEffect(Gdx.graphics.getWidth() / 10, Gdx.graphics.getHeight() / 2, 0.001f);
//		RedNeonEffectForJoker.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 1f);
//		RedNeonEffectForJoker1.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 1f);
//		RedNeonEffectForJoker2.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 1f);
//		RedNeonEffectForJoker3.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 1f);
//		
//		
	//RedNeonEffect.getClass().
	this.DecideRandomNeon = DecideRandomNeon;

	//////////////////////////////
	NeonBlueRect = new Rectangle(0, 0, Gdx.graphics.getWidth() / 30, Gdx.graphics.getHeight() / 2.75f);
	NeonRedRect = new Rectangle(Gdx.graphics.getWidth() - Gdx.graphics.getWidth() / 30, 0, Gdx.graphics.getHeight() / 30, Gdx.graphics.getHeight() / 2.75f);
	NeonYellowRect = new Rectangle(0, Gdx.graphics.getHeight() / 2.5f, Gdx.graphics.getWidth() / 30, Gdx.graphics.getHeight() / 2.5f);
	NeonGreenRect = new Rectangle(Gdx.graphics.getWidth() - Gdx.graphics.getWidth() / 30, Gdx.graphics.getHeight() / 2, Gdx.graphics.getWidth() / 30, Gdx.graphics.getHeight() / 2.5f);

	RenkOfNeon1 = 1;
	BlueNeonEffect.setPosition(Gdx.graphics.getWidth() - Gdx.graphics.getWidth() / 35, Gdx.graphics.getHeight() / 8);

	//1 kırmızı 2 mavi


	RedNeonEffect.setPosition(Gdx.graphics.getWidth() / 35, Gdx.graphics.getHeight() / 8);
//		YellowNeonEffect.setPosition(Gdx.graphics.getWidth()/35, Gdx.graphics.getHeight()/2 + Gdx.graphics.getHeight()/);
	YellowNeonEffect.setPosition(Gdx.graphics.getWidth() / 35, Gdx.graphics.getHeight() / 2 + Gdx.graphics.getWidth() / 8);


	GreenNeonEffect.setPosition(Gdx.graphics.getWidth() - Gdx.graphics.getWidth() / 35, Gdx.graphics.getHeight() / 2 + Gdx.graphics.getWidth() / 8);

	RenkOfNeon2 = 2;
}

//default olarak neon1 solda, neon2 sağda9
	@Override
	public void render(SpriteBatch SB) {

		if(Count> 5350) {
			/**************************************/
			RedNeonEffect.load(Gdx.files.internal("redneon1.p"), Gdx.files.internal(""));
			RedNeonEffect.start();

			//YellowNeonEffect = new ParticleEffect();
			YellowNeonEffect.load(Gdx.files.internal("sarilarinsulo.p"), Gdx.files.internal(""));
			YellowNeonEffect.start();

			//BlueNeonEffect = new ParticleEffect();
			BlueNeonEffect.load(Gdx.files.internal("blue3.p"), Gdx.files.internal(""));
			BlueNeonEffect.start();

			//GreenNeonEffect = new ParticleEffect();
			GreenNeonEffect.load(Gdx.files.internal("greenneon.p"), Gdx.files.internal(""));
			GreenNeonEffect.start();
			/*
			//RedNeonEffect = new ParticleEffect();
			  RedNeonEffect.load(Gdx.files.internal("redneon1.p"), Gdx.files.internal(""));
			  RedNeonEffect.start();


			  //YellowNeonEffect = new ParticleEffect();
			  YellowNeonEffect.load(Gdx.files.internal("sarilarinsulo.p"), Gdx.files.internal(""));
			  YellowNeonEffect.start();
			  
				//BlueNeonEffect = new ParticleEffect();
				BlueNeonEffect.load(Gdx.files.internal("blue3.p"), Gdx.files.internal(""));
				BlueNeonEffect.start();
				
				//GreenNeonEffect = new ParticleEffect();
				GreenNeonEffect.load(Gdx.files.internal("greenneon.p"), Gdx.files.internal(""));
				GreenNeonEffect.start();







			/////////////////////////////////
			//JokerCount = 0;
//				RedNeonEffectForJoker = new ParticleEffect();
//				  RedNeonEffectForJoker.load(Gdx.files.internal("redneon1.p"), Gdx.files.internal(""));
//				  RedNeonEffectForJoker.start();
//				  
//				  RedNeonEffectForJoker1 = new ParticleEffect();
//				  RedNeonEffectForJoker1.load(Gdx.files.internal("redneon1.p"), Gdx.files.internal(""));
//				  RedNeonEffectForJoker1.start();
//				  
//				  RedNeonEffectForJoker2 = new ParticleEffect();
//				  RedNeonEffectForJoker2.load(Gdx.files.internal("redneon1.p"), Gdx.files.internal(""));
//				  RedNeonEffectForJoker2.start();
//				  
//				  RedNeonEffectForJoker3 = new ParticleEffect();
//				  RedNeonEffectForJoker3.load(Gdx.files.internal("redneon1.p"), Gdx.files.internal(""));
//				  RedNeonEffectForJoker3.start();

				BlueNeonEffect.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 0.0001f);
				GreenNeonEffect.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 0.0001f);
				RedNeonEffect.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 0.0001f);
				YellowNeonEffect.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 0.0001f);
//				RedNeonEffectForJoker.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 1f);
//				RedNeonEffectForJoker1.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 1f);
//				RedNeonEffectForJoker2.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 1f);
//				RedNeonEffectForJoker3.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 1f);
//				
//				

				//RedNeonEffect.getClass().
				*/


			BlueNeonEffect.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 0.001f);
			GreenNeonEffect.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 0.001f);
			RedNeonEffect.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 0.001f);
			YellowNeonEffect.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 0.001f);

			Count = 0;

		}

		Count++;
//		SR.begin(ShapeType.Filled);
//		SR.setColor(Color.RED);
//		SR.rect(0, 0, Gdx.graphics.getWidth()/30, Gdx.graphics.getHeight()/3);
//		SR.end();
		SB.begin();
		GreenNeonEffect.draw(SB);
		SB.end();
		
		if(GreenNeonEffect.isComplete() ) {
			GreenNeonEffect.reset();
		}

		
		
		
		SB.begin();
		BlueNeonEffect.draw(SB);
		SB.end();
		
		if(BlueNeonEffect.isComplete() ) {
			BlueNeonEffect.reset();
		}
		
		
		
		
		
		
		SB.begin();
		RedNeonEffect.draw(SB);
		SB.end();
		if(RedNeonEffect.isComplete() ) {
			RedNeonEffect.reset();
			System.out.println("RESETLEDİ");
		}
		
		
		
		
		SB.begin();
		YellowNeonEffect.draw(SB);
		SB.end();
		if(YellowNeonEffect.isComplete()) {
			YellowNeonEffect.reset();
		}

		 if(DecideRandomNeon<5) {/* < 5
			NeonRedRect = new Rectangle(0, 0, Gdx.graphics.getWidth()/30, Gdx.graphics.getHeight()/2.75f);
			NeonBlueRect = new Rectangle(Gdx.graphics.getWidth() - Gdx.graphics.getWidth()/30,0,Gdx.graphics.getHeight()/30,Gdx.graphics.getHeight()/2.75f);
			NeonYellowRect = new Rectangle(Gdx.graphics.getWidth() - Gdx.graphics.getWidth()/30,Gdx.graphics.getHeight()/2.5f,Gdx.graphics.getWidth()/30, Gdx.graphics.getHeight()/2);
			NeonGreenRect = new Rectangle(0, Gdx.graphics.getHeight()/2.5f, Gdx.graphics.getWidth()/30, Gdx.graphics.getHeight()/2);
*/

		NeonRedRect.setPosition(Gdx.graphics.getWidth()/35,  Gdx.graphics.getHeight()/8);

		NeonBlueRect.setPosition(Gdx.graphics.getWidth()- Gdx.graphics.getWidth()/35,  Gdx.graphics.getHeight()/8);

		NeonYellowRect.setPosition(Gdx.graphics.getWidth()/35, Gdx.graphics.getHeight()/2+ Gdx.graphics.getWidth()/8);

		NeonGreenRect.setPosition(Gdx.graphics.getWidth()- Gdx.graphics.getWidth()/35, Gdx.graphics.getHeight()/2 + Gdx.graphics.getWidth()/8);



			 //kırmızı sol alt mavi sağ alt, yeşil sol üst,sarı sağ üst

		RenkOfNeon1 = 1;
			 BlueNeonEffect.setPosition(Gdx.graphics.getWidth()- Gdx.graphics.getWidth()/35,  Gdx.graphics.getHeight()/8);

			 //1 kırmızı 2 mavi


		RedNeonEffect.setPosition(Gdx.graphics.getWidth()/35,  Gdx.graphics.getHeight()/8);
//		YellowNeonEffect.setPosition(Gdx.graphics.getWidth()/35, Gdx.graphics.getHeight()/2 + Gdx.graphics.getHeight()/);
		YellowNeonEffect.setPosition(Gdx.graphics.getWidth()/35, Gdx.graphics.getHeight()/2+ Gdx.graphics.getWidth()/8);


		GreenNeonEffect.setPosition(Gdx.graphics.getWidth()- Gdx.graphics.getWidth()/35, Gdx.graphics.getHeight()/2 + Gdx.graphics.getWidth()/8);

		RenkOfNeon2 = 2;
		
		
		}
		else if(DecideRandomNeon >= 5) {
			//mavi sol alt, kırmızı sağ alt, yeşil sağ üst, sarı sol üst
			NeonBlueRect.setPosition(Gdx.graphics.getWidth()/35,  Gdx.graphics.getHeight()/8);
			 NeonRedRect.setPosition(Gdx.graphics.getWidth()- Gdx.graphics.getWidth()/35,  Gdx.graphics.getHeight()/8);
			 NeonGreenRect.setPosition(Gdx.graphics.getWidth()/35, Gdx.graphics.getHeight()/2+ Gdx.graphics.getWidth()/8);
			 NeonYellowRect.setPosition(Gdx.graphics.getWidth()- Gdx.graphics.getWidth()/35, Gdx.graphics.getHeight()/2 + Gdx.graphics.getWidth()/8);


		RedNeonEffect.setPosition(Gdx.graphics.getWidth()- Gdx.graphics.getWidth()/35,  Gdx.graphics.getHeight()/8);
		BlueNeonEffect.setPosition(Gdx.graphics.getWidth()/35,  Gdx.graphics.getHeight()/8);
		

		YellowNeonEffect.setPosition(Gdx.graphics.getWidth()- Gdx.graphics.getWidth()/35, Gdx.graphics.getHeight()/2 + Gdx.graphics.getWidth()/8);
		
		GreenNeonEffect.setPosition(Gdx.graphics.getWidth()/35, Gdx.graphics.getHeight()/2+ Gdx.graphics.getWidth()/8);

		}
		
		
		
	}

	public Rectangle getNeonRedRect() {
		return NeonRedRect;
	}
	public void setNeonRedRect(Rectangle neonRedRect) {
		NeonRedRect = neonRedRect;
	}
//	public boolean isJokerBall() {
//		return JokerBall;
//	}
//	public void setJokerBall(boolean jokerBall) {
//		JokerBall = jokerBall;
//	}
	@Override
	public void update(float delta) {
		BlueNeonEffect.update(delta);


RedNeonEffect.update(delta);
YellowNeonEffect.update(delta);
GreenNeonEffect.update(delta);


		
//		if(JokerBall) {
//RedNeonEffectForJoker.update(delta);
//RedNeonEffectForJoker1.update(delta);
//RedNeonEffectForJoker2.update(delta);
//RedNeonEffectForJoker3.update(delta);
//		}
}

	@Override
	public void destroyElement() {
		RedNeonEffect.dispose();
		BlueNeonEffect.dispose();
		YellowNeonEffect.dispose();
		GreenNeonEffect.dispose();
        NeonGreenRect = null;
        NeonYellowRect = null;
        NeonBlueRect = null;
        NeonRedRect = null;
	}

	public  void SetDecideNeonColor(int RandomNeon) {
		DecideRandomNeon = RandomNeon;
		
	}

public Rectangle getNeon1Rect() {
	return NeonRedRect;
}
public void setNeon1Rect(Rectangle neon1Rect) {
	NeonRedRect = neon1Rect;
}
public Rectangle getNeon2Rect() {
	return NeonBlueRect;
}
public byte getRenkOfNeon2() {
	return RenkOfNeon2;
}
public Rectangle getNeonYellowRect() {
	return NeonYellowRect;
}
public void setNeonYellowRect(Rectangle neonYellowRect) {
	NeonYellowRect = neonYellowRect;
}
public Rectangle getNeonGreenRect() {
	return NeonGreenRect;
}
public void setNeonGreenRect(Rectangle neonGreenRect) {
	NeonGreenRect = neonGreenRect;
}
public void setRenkOfNeon2(byte renkOfNeon2) {
	RenkOfNeon2 = renkOfNeon2;
}
public void setNeon2Rect(Rectangle neon2Rect) {
	NeonBlueRect = neon2Rect;
}
public byte getRenkOfNeon1() {
	return RenkOfNeon1;
}
public void setRenkOfNeon1(byte renkOfNeon1) {
	RenkOfNeon1 = renkOfNeon1;
}

}
