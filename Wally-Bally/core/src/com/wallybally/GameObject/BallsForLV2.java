package com.wallybally.GameObject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Circle;
import com.wallybally.ImageLoader.*;
import com.wallybally.states.*;

public class BallsForLV2 implements GameOBject{
	public Circle BallsCircle;
	
	private float FirstXKordOfBalls;
	private float FirstYKordOfBalls;
	private  float BallsSpeed;
	private  float BallsAc;
	private float BallsSpeedX;
	private float BallsSpeedY;
	private float x;
	private float y;
	private float r;
	private float XkordBalls;
	private float yKordBalls;
	private float WidthBalls;
	private float HeightBalls;
	public int DecideBallColor;
	private boolean CarPistim;
	private boolean BandaCarptim;
	private boolean NeonaCarptim;
	private boolean PushParticleEffect;

	private boolean DusmeIslemi;
	private byte Renk; //1 = red , 2= blue, 3 = yellow, 4 = green
	 public BallsForLV2(float xkordBalls, float yKordBalls, float WidthBalls, float HeightBalls
) {
		 PushParticleEffect = false;
	r = WidthBalls/2;
	CarPistim = false;
	BallsSpeedX = 0;
	BallsSpeedY = 0;
	this.XkordBalls = xkordBalls;
	this.yKordBalls = yKordBalls;
	this.WidthBalls = WidthBalls;
	this.HeightBalls = HeightBalls;
	x = xkordBalls+(WidthBalls/2);
	y = yKordBalls+ (HeightBalls/2);
	BallsCircle = new Circle(x, y, r);
	FirstXKordOfBalls = xkordBalls;
	BallsAc = 0.1f;
	System.out.println();
	FirstYKordOfBalls = yKordBalls;
	BandaCarptim = false;
	NeonaCarptim = false;

	DusmeIslemi = false;
//	
	 }



	@Override
	public void render(SpriteBatch SB) {

		SB.begin();


		ImageLoader.CollisionToNeonEffect.draw(SB);
		SB.end();

		if(!isNeonaCarptim()) {
			if(ImageLoader.CollisionToNeonEffect.isComplete()) {

				ImageLoader.CollisionToNeonEffect.reset();
			}
		}


		SB.begin();
		ImageLoader.CollisionToMainEffect.draw(SB);
		SB.end();


		if(ImageLoader.CollisionToMainEffect.isComplete() && isCarPistim()) {
			ImageLoader.CollisionToMainEffect.reset();
		}
		
		if(NeonaCarptim) {
			
		}

		if(DecideBallColor <3 && !NeonaCarptim) {
			BlueBall(SB);
			Renk = 2;
		}
		if(DecideBallColor>= 3 && DecideBallColor <= 6 && !NeonaCarptim) {
			RedBall(SB);
			Renk = 1;
		}
		if(DecideBallColor > 6 && !NeonaCarptim) {
			GreenBall(SB);
		}
		
//		if(DecideBallColor>6) {    FOR UPPER LEVEL
//			RedBall(SB);
//		}
	}

	@Override
	public void update(float delta) {
	//	XkordBalls+= 1;
	if(isNeonaCarptim()) {
		ImageLoader.CollisionToNeonEffect.setPosition(this.x, this.y);

		ImageLoader.CollisionToNeonEffect.update(delta);
	}
		
		
		if(PushParticleEffect) {
			ImageLoader.CollisionToMainEffect.setPosition(this.x,this.y);

			ImageLoader.CollisionToMainEffect.update(delta);
		}	x = XkordBalls+(WidthBalls/2);
		y = yKordBalls+ (HeightBalls/2);
		BallsCircle.x = x;
		BallsCircle.y = y;


		BallsSpeedY -= 0.05f;
		yKordBalls += BallsSpeedY;
		XkordBalls += BallsSpeedX;

		//System.out.println("Speed YY"+ BallsSpeedY);
		
	}

	@Override
	public void destroyElement() {
		BallsCircle = null;
	}

	public float getR() {
		return r;
	}



	public void setR(float r) {
		this.r = r;
	}



	public void RedBall(SpriteBatch SB) {
		
		SB.begin();
		SB.draw(ImageLoader.RedBallReg, XkordBalls, yKordBalls, WidthBalls, HeightBalls);
		SB.end();
	}
	
	public void GreenBall(SpriteBatch SB) {
		SB.begin();
		SB.draw(ImageLoader.GreenBallReg, XkordBalls, yKordBalls, WidthBalls, HeightBalls);
		SB.end();
		
	}
	
	public void BlueBall(SpriteBatch SB) {
		
		SB.begin();
		SB.draw(ImageLoader.BlueBallReg, XkordBalls, yKordBalls, WidthBalls, HeightBalls);
		SB.end();
	}
	
	public void YellowBall(SpriteBatch SB) {
		SB.begin();
		SB.draw(ImageLoader.YellowBallReg, XkordBalls, yKordBalls, WidthBalls, HeightBalls);
		SB.end();
	}
	
	public void SetDecideBallColor(int RandomXInGameArea) {
		this.DecideBallColor = RandomXInGameArea;
		
	}
	
	
	
	
	
	
	
	
	public Circle getCollisionCircle() {
		
		return BallsCircle;
	}
	
	
	
	
	public float getFirstXKordOfBalls() {
		return FirstXKordOfBalls;
	}
	public void setFirstXKordOfBalls(float firstXKordOfBalls) {
		FirstXKordOfBalls = firstXKordOfBalls;
	}
	public float getFirstYKordOfBalls() {
		return FirstYKordOfBalls;
	}
	public void setFirstYKordOfBalls(float firstYKordOfBalls) {
		FirstYKordOfBalls = firstYKordOfBalls;
	}
	
	
	public float getBallsSpeedX() {
		return BallsSpeedX;
	}

	public void setBallsSpeedX(float ballsSpeedX) {
		this.BallsSpeedX = ballsSpeedX;
	}

	public float getBallsSpeedY() {
		return BallsSpeedY;
	}

	public void setBallsSpeedY(float ballsSpeedY) {
		this.BallsSpeedY = ballsSpeedY;
	}
	

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public boolean isDusmeIslemi() {
		return DusmeIslemi;
	}

	public void setDusmeIslemi(boolean dusmeIslemi) {
		DusmeIslemi = dusmeIslemi;
	}

	public boolean isPushParticleEffect() {
		return PushParticleEffect;
	}

	public void setPushParticleEffect(boolean pushParticleEffect) {
		PushParticleEffect = pushParticleEffect;
	}

	public byte getRenk() {
		return Renk;
	}



	public void setRenk(byte renk) {
		Renk = renk;
	}



	public boolean isNeonaCarptim() {
		return NeonaCarptim;
	}



	public void setNeonaCarptim(boolean neonaCarptim) {
		NeonaCarptim = neonaCarptim;
	}



	public boolean isBandaCarptim() {
		return BandaCarptim;
	}



	public void setBandaCarptim(boolean bandaCarptim) {
		BandaCarptim = bandaCarptim;
	}



	public boolean isCarPistim() {
		return CarPistim;
	}



	public void setCarPistim(boolean carPistim) {
		CarPistim = carPistim;
	}

	
	
	
	
}
