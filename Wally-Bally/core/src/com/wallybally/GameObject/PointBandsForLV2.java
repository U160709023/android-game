package com.wallybally.GameObject;

// siyah top collisionda can götürür
//beyaz top vurulduğunda bütün renkler eşit olur seri seri top gelir
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.wallybally.ImageLoader.*;
import com.wallybally.GameObject.*;


public class PointBandsForLV2 implements GameOBject{

private static int DecideRandomNeon;
private Rectangle Neon1Rect;
private Rectangle Neon2Rect;
private byte RenkOfNeon1; //1 = red, 2= blue, 3= yellow, 4 = green(Sol)
private byte RenkOfNeon2; // Sağ
private ParticleEffect RedNeonEffect;
private ParticleEffect BlueNeonEffect;
int Count;
public PointBandsForLV2(int DecideRandomNeon) {
	Count = 5350;
	RedNeonEffect = new ParticleEffect();
	  RedNeonEffect.load(Gdx.files.internal("redneon1.p"), Gdx.files.internal(""));
	  RedNeonEffect.start();

		BlueNeonEffect = new ParticleEffect();
		BlueNeonEffect.load(Gdx.files.internal("blue3.p"), Gdx.files.internal(""));
		BlueNeonEffect.start();
		RedNeonEffect.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight(), 0.01f);
		BlueNeonEffect.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight(), 0.01f);

	this.DecideRandomNeon = DecideRandomNeon;
	
	Neon1Rect = new Rectangle(0, Gdx.graphics.getHeight()/6, Gdx.graphics.getWidth()/30, Gdx.graphics.getHeight()/2.5f);
	Neon2Rect = new Rectangle(Gdx.graphics.getWidth() - Gdx.graphics.getWidth()/30, Gdx.graphics.getHeight()/6, Gdx.graphics.getWidth()/30, Gdx.graphics.getHeight()/2.5f);//(Gdx.graphics.getHeight()/25)

	//default olarak neon1 solda, neon2 sağda
}
	@Override
	public void render(SpriteBatch SB) {

		if(Count> 5350) {
			/**************************************/
			RedNeonEffect.load(Gdx.files.internal("redneon1.p"), Gdx.files.internal(""));
			RedNeonEffect.start();

			//YellowNeonEffect = new ParticleEffect();

			//BlueNeonEffect = new ParticleEffect();
			BlueNeonEffect.load(Gdx.files.internal("blue3.p"), Gdx.files.internal(""));
			BlueNeonEffect.start();



			BlueNeonEffect.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 0.001f);
			RedNeonEffect.scaleEffect(Gdx.graphics.getWidth()/10, Gdx.graphics.getHeight()/2, 0.001f);
			Count = 0;

		}
		Count++;
//sol
		SB.begin();
	//	SB.draw(ImageLoader.myBandBlue,0, Gdx.graphics.getHeight()/6, Gdx.graphics.getWidth()/30, Gdx.graphics.getHeight()/2.5f);
	//	SB.draw(ImageLoader.myBandBlue,Gdx.graphics.getWidth() - Gdx.graphics.getWidth()/30, Gdx.graphics.getHeight()/6, Gdx.graphics.getWidth()/30, Gdx.graphics.getHeight()/2.5f);//(Gdx.graphics.getHeight()/25)

		SB.end();


		SB.begin();
		BlueNeonEffect.draw(SB);
		SB.end();
		
		if(BlueNeonEffect.isComplete()) {
			BlueNeonEffect.reset();
		}
		
		SB.begin();
		RedNeonEffect.draw(SB);
		SB.end();
		if(RedNeonEffect.isComplete()) {
			RedNeonEffect.reset();
		}

		if(DecideRandomNeon<5) {
//		NeonRed.begin();//kırmızı solda mavi sağda
//		NeonRed.setColor(Color.RED);
//		NeonRed.rect(0, 0, Gdx.graphics.getWidth()/30, Gdx.graphics.getHeight() -(Gdx.graphics.getHeight()/25));
//		NeonRed.end();

		//1 kırmızı 2 mavi
		Neon1Rect.x = 0;
//		Neon1Rect.y = 0;
		Neon2Rect.x =Gdx.graphics.getWidth() - Gdx.graphics.getWidth()/30;
//		Neon2Rect.y =0;


			//burası
		RedNeonEffect.setPosition(Gdx.graphics.getWidth()/35,  Gdx.graphics.getHeight()/2.75f);
		BlueNeonEffect.setPosition(Gdx.graphics.getWidth()- Gdx.graphics.getWidth()/35,  Gdx.graphics.getHeight()/2.75f);



		//	NeonBlue.begin(ShapeType.Filled);
//		NeonBlue.setColor(Color.BLUE);
//		NeonBlue.rect(Gdx.graphics.getWidth() - Gdx.graphics.getWidth()/30, 0, Gdx.graphics.getWidth()/30, Gdx.graphics.getHeight() -(Gdx.graphics.getHeight()/25));
//		NeonBlue.end();


			RenkOfNeon1 = 1;
		RenkOfNeon2 = 2;
			
		//burası düzeltilecek
		//Neon1Rect = new Rectangle(0, 0, Gdx.graphics.getWidth()/30, Gdx.graphics.getHeight() -(Gdx.graphics.getHeight()/25));
		//Neon2Rect = new Rectangle(Gdx.graphics.getWidth() - Gdx.graphics.getWidth()/30, 0, Gdx.graphics.getWidth()/30, Gdx.graphics.getHeight() -(Gdx.graphics.getHeight()/25));
	
		
		}
		if(DecideRandomNeon >= 5) {
//		NeonBlue.begin(ShapeType.Filled); //mavi solda, kırmıı sağda
//		NeonBlue.setColor(Color.BLUE);
//		NeonBlue.rect(0, 0, Gdx.graphics.getWidth()/30, Gdx.graphics.getHeight() -(Gdx.graphics.getHeight()/25));
//		NeonBlue.end();
		RenkOfNeon1 = 2;
		RenkOfNeon2 = 1;


		RedNeonEffect.setPosition(Gdx.graphics.getWidth()- Gdx.graphics.getWidth()/35,  Gdx.graphics.getHeight()/2.75f);
		BlueNeonEffect.setPosition(Gdx.graphics.getWidth()/35,  Gdx.graphics.getHeight()/2.75f);
			Neon1Rect.x = Gdx.graphics.getWidth() - Gdx.graphics.getWidth()/30;
//		Neon1Rect.y = 0;
			Neon2Rect.x =0;
//		Neon2Rect.y =0;

		//	Neon1Rect = new Rectangle(Gdx.graphics.getWidth() - Gdx.graphics.getWidth()/30, 0, Gdx.graphics.getWidth()/30, Gdx.graphics.getHeight() -(Gdx.graphics.getHeight()/25));
		//Neon2Rect = new Rectangle(0, 0, Gdx.graphics.getWidth()/30, Gdx.graphics.getHeight() -(Gdx.graphics.getHeight()/25));

		}


	}

	@Override
	public void update(float delta) {
RedNeonEffect.update(delta);
BlueNeonEffect.update(delta);
	}

	@Override
	public void destroyElement() {
		RedNeonEffect.dispose();
		BlueNeonEffect.dispose();
		Neon1Rect = null;
		Neon2Rect = null;
	}

	public static void SetDecideNeonColor(int RandomNeon) {
		DecideRandomNeon = RandomNeon;
		
	}

public Rectangle getNeon1Rect() {
	return Neon1Rect;
}
public void setNeon1Rect(Rectangle neon1Rect) {
	Neon1Rect = neon1Rect;
}
public Rectangle getNeon2Rect() {
	return Neon2Rect;
}
public byte getRenkOfNeon2() {
	return RenkOfNeon2;
}
public void setRenkOfNeon2(byte renkOfNeon2) {
	RenkOfNeon2 = renkOfNeon2;
}
public void setNeon2Rect(Rectangle neon2Rect) {
	Neon2Rect = neon2Rect;
}
public byte getRenkOfNeon1() {
	return RenkOfNeon1;
}
public void setRenkOfNeon1(byte renkOfNeon1) {
	RenkOfNeon1 = renkOfNeon1;
}

}
