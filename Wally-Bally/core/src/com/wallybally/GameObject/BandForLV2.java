package com.wallybally.GameObject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.wallybally.ImageLoader.*;

public class BandForLV2 implements GameOBject{
	private float Band1Xkord;
	private float Band1Ykord;
	private float Band1Width;
	private float Band1Heght;
	
	private float Band2Xkord;
	private float Band2Ykord;
	private float Band2Width;
	private float Band2Height;
	
	private float Band3XKord;
	private float Band3YKord;
	private float Band3Width;
	private float Band3Height;
	
	
	

	private Rectangle Band1;
	private Rectangle Band2;
	private Rectangle Band3;
	public BandForLV2(float Band1Xkord, float Band1Ykord, float Band1Width, float Band1Height,
			float Band2Xkord, float Band2Ykord, float Band2Width, float Band2Height,
			float Band3XKord, float Band3YKord, float Band3Width, float Band3Height
			
			) {
		this.Band1Xkord = Band1Xkord;
		this.Band1Ykord = Band1Ykord;
		this.Band1Width = Band1Width;
		this.Band1Heght = Band1Height;
		
		this.Band2Xkord = Band2Xkord;
		this.Band2Ykord = Band2Ykord;
		this.Band2Width = Band2Width;
		this.Band2Height = Band2Height;
		
		
		this.Band3XKord = Band3XKord;
		this.Band3YKord = Band3YKord;
		this.Band3Width = Band3Width;
		this.Band3Height = Band3Height;
		Band1 = new Rectangle(Band1Xkord ,Band1Ykord, Band1Width,Band1Height);
		Band2 = new Rectangle(Band2Xkord, Band2Ykord,Band2Width,Band2Height);
		Band3 = new Rectangle(Band3XKord, Band3YKord, Band3Width, Band3Height);
	
	}
	public Rectangle getBand3() {
		return Band3;
	}
	public void setBand3(Rectangle band3) {
		Band3 = band3;
	}
	public Rectangle getBand1() {
		return Band1;
	}
	public void setBand1(Rectangle band1) {
		Band1 = band1;
	}
	public Rectangle getBand2() {
		return Band2;
	}
	public void setBand2(Rectangle band2) {
		Band2 = band2;
	}
	@Override
	public void render(SpriteBatch SB) {
		SB.begin();
		SB.draw(ImageLoader.BantOfGame, this.Band1Xkord,this.Band1Ykord, this.Band1Width,this.Band1Heght);
	//	SB.draw(ImageLoader.BantOfGameReg, this.Band1Xkord, this.Band1Ykord, this.Band1Width, this.Band1Heght);
		//SB.draw(ImageLoader.BantOfGameReg, this.Band1Xkord, Gdx.graphics.getHeight() - Gdx.graphics.getHeight()/20, this.Band1Xkord, Gdx.graphics.getHeight() - Gdx.graphics.getHeight()/20, 100, 30, 30, 30, 90.0f, true);
		SB.end();
		
		
		
		SB.begin();
		SB.draw(ImageLoader.BantOfGame, this.Band2Xkord, this.Band2Ykord, this.Band2Width, Gdx.graphics.getHeight()/10);
	//	SB.draw(ImageLoader.BantOfGameReg, this.Band2Xkord, this.Band2Ykord, this.Band2Width, this.Band2Height);
		
		SB.end();
		SB.begin();
		SB.draw(ImageLoader.BantOfGame, this.Band3XKord, this.Band3YKord, this.Band3Width, this.Band3Height);
	//	SB.draw(ImageLoader.BantOfGameReg, this.Band2Xkord, this.Band2Ykord, this.Band2Width, this.Band2Height);
		
		SB.end();
		

		
		
//		SB.begin();
//		SB.draw(ImageLoader.BantOfGameReg, 0, Gdx.graphics.getHeight() - (Gdx.graphics.getHeight()/25), Gdx.graphics.getWidth(), Gdx.graphics.getHeight()/25);
//		SB.end();
	}

	@Override
	public void update(float delta) {

		
	}

	@Override
	public void destroyElement() {
			Band1 = null;
			Band2 = null;
	}

}
