package com.wallybally.GameObject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.wallybally.ImageLoader.*;

public class Band implements GameOBject{
	private float Band1Xkord;
	private float Band1Ykord;
	private float Band1Width;
	private float Band1Heght;
	
	private float Band2Xkord;
	private float Band2Ykord;
	private float Band2Width;
	private float Band2Height;
	
	
	

	private Rectangle Band1;
	private Rectangle Band2;
	public Band(float Band1Xkord, float Band1Ykord, float Band1Width, float Band1Height,
			float Band2Xkord, float Band2Ykord, float Band2Width, float Band2Height) {
		this.Band1Xkord = Band1Xkord;
		this.Band1Ykord = Band1Ykord;
		this.Band1Width = Band1Width;
		this.Band1Heght = Band1Height;
		
		this.Band2Xkord = Band2Xkord;
		this.Band2Ykord = Band2Ykord;
		this.Band2Width = Band2Width;
		this.Band2Height = Band2Height;
		Band1 = new Rectangle(this.Band1Xkord, Gdx.graphics.getHeight() - Gdx.graphics.getHeight()/20, Gdx.graphics.getWidth()*(9)/20, Gdx.graphics.getHeight()/10);
		Band2 = new Rectangle(Gdx.graphics.getWidth()*(9)/20 + (Gdx.graphics.getWidth()/10),Gdx.graphics.getHeight() - (Gdx.graphics.getHeight()/20), Gdx.graphics.getWidth()*(9)/20, Gdx.graphics.getHeight()/10);
	}
	public Rectangle getBand1() {
		return Band1;
	}
	public void setBand1(Rectangle band1) {
		Band1 = band1;
	}
	public Rectangle getBand2() {
		return Band2;
	}
	public void setBand2(Rectangle band2) {
		Band2 = band2;
	}
	@Override
	public void render(SpriteBatch SB) {
		SB.begin();
		SB.draw(ImageLoader.BantOfGame, this.Band1Xkord, Gdx.graphics.getHeight() - Gdx.graphics.getHeight()/20, Gdx.graphics.getWidth()*(9)/20, Gdx.graphics.getHeight()/10);
	//	SB.draw(ImageLoader.BantOfGameReg, this.Band1Xkord, this.Band1Ykord, this.Band1Width, this.Band1Heght);
		//SB.draw(ImageLoader.BantOfGameReg, this.Band1Xkord, Gdx.graphics.getHeight() - Gdx.graphics.getHeight()/20, this.Band1Xkord, Gdx.graphics.getHeight() - Gdx.graphics.getHeight()/20, 100, 30, 30, 30, 90.0f, true);
		SB.end();
		SB.begin();
		SB.draw(ImageLoader.BantOfGame, Gdx.graphics.getWidth()*(9)/20 + (Gdx.graphics.getWidth()/10),Gdx.graphics.getHeight() - (Gdx.graphics.getHeight()/20), Gdx.graphics.getWidth()*(9)/20, Gdx.graphics.getHeight()/10);
	//	SB.draw(ImageLoader.BantOfGameReg, this.Band2Xkord, this.Band2Ykord, this.Band2Width, this.Band2Height);
		
		SB.end();
		
//		SB.begin();
//		SB.draw(ImageLoader.BantOfGameReg, 0, Gdx.graphics.getHeight() - (Gdx.graphics.getHeight()/25), Gdx.graphics.getWidth(), Gdx.graphics.getHeight()/25);
//		SB.end();
	}

	@Override
	public void update(float delta) {

		
	}

	@Override
	public void destroyElement() {
		Band1 = null;
		Band2 = null;
	}

}
