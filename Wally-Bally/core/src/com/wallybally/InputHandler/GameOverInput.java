package com.wallybally.InputHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.wallybally.game.WallyBally;
import com.wallybally.states.GameOverState;
import com.wallybally.states.PlayState;
import com.wallybally.states.StateManager;

public class GameOverInput implements InputProcessor {

	private GameOverState GO;
	public static boolean isClickedRestartButton = false;
	int Level;
	public static boolean clickedWatchVideoButton = false;
	public GameOverInput(GameOverState GO,int Level) {
		this.GO = GO;this.Level = Level;
	}
	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {

		if(GO.getReplayButton().contains(screenX, Gdx.graphics.getHeight() - screenY)) {
			isClickedRestartButton = true;
			PlayState.isGameEnd = false;
			GO.disposeElements();
			GO.getSm().popState();
			GO.getSm().PushState(new PlayState(GO.getSm(), Level));
		}
		else if(GO.getWatchVideoButton().contains(screenX, Gdx.graphics.getHeight() - screenY)){
			clickedWatchVideoButton = true;
			System.out.println("helal lan amk");
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
