package com.wallybally.InputHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.wallybally.game.WallyBally;
import com.wallybally.states.LevelState;
import com.wallybally.states.MenuState;
import com.wallybally.states.PlayState;

public class MenuStateInput implements InputProcessor {
	private MenuState MS;
	int Level;
	public MenuStateInput(MenuState MS, int Level) {

		this.MS = MS;
		this.Level = Level;
	
	}
	
	
	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if(MS.getStartButtonRect().contains(screenX, Gdx.graphics.getHeight() -screenY)){
            MenuState.isClickedStarButton = true;
			MS.disposeElements();
			MS.getSm().PushState(new PlayState(MS.getSm(), MS.level));

		}
		else if (MS.getLevelButtonRect().contains(screenX,Gdx.graphics.getHeight() - screenY )){
			MenuState.isClickedStarButton = true;
			MS.getSm().PushState(new LevelState(MS.getSm(), Level));
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
