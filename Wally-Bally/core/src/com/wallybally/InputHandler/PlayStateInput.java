package com.wallybally.InputHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

import com.wallybally.GameObject.MainPlayer;
import com.wallybally.states.PlayState;

public class PlayStateInput implements InputProcessor{
	public float UzaklıkX;
	public float UzaklıkY;
	private PlayState PS;
	private boolean StartPointToCreateBall = false;
	private static boolean isTouchOnArea = false;
	public static int ENandBoy;

	int Level;
	public PlayStateInput(PlayState PS,int Level) {//PS input olarak alınır, içerisinde işlem alınıp alınmadığını kontrol eder
		this.PS = PS;
		ENandBoy = Gdx.graphics.getWidth()/10;
		this.Level = Level;
	}
	
	@Override
	public boolean keyDown(int keycode) {
		//System.out.println("bir tuşa basıldı");	
	return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		
		if(Level == 1 || LevelStateInput.OnWhichLevel == 1) {
			if (!PS.getGameArea().getArrowCreateArea().contains(screenX, Gdx.graphics.getHeight() - screenY)) {
				StartPointToCreateBall = false;

			}
			if (PS.getGameArea().getArrowCreateArea().contains(screenX, Gdx.graphics.getHeight() - screenY)) {
				StartPointToCreateBall = true;
				isTouchOnArea = true;
				PS.getGameArea().PushNewBall(new MainPlayer(screenX - ((Gdx.graphics.getWidth() / 10) - (Gdx.graphics.getWidth() / 20)), (Gdx.graphics.getHeight() - screenY) - ((Gdx.graphics.getWidth() / 10) - (Gdx.graphics.getWidth() / 20)), ENandBoy, ENandBoy));
				PS.CreateBall = true;


			}
		}
		else if(Level == 2|| LevelStateInput.OnWhichLevel == 2){
			if (!PS.getGameArea2().getArrowCreateArea().contains(screenX, Gdx.graphics.getHeight() - screenY)) {
				StartPointToCreateBall = false;

			}
			if (PS.getGameArea2().getArrowCreateArea().contains(screenX, Gdx.graphics.getHeight() - screenY)) {
				StartPointToCreateBall = true;
				isTouchOnArea = true;
				PS.getGameArea2().PushNewBall(new MainPlayer(screenX - ((Gdx.graphics.getWidth() / 10) - (Gdx.graphics.getWidth() / 20)), (Gdx.graphics.getHeight() - screenY) - ((Gdx.graphics.getWidth() / 10) - (Gdx.graphics.getWidth() / 20)), ENandBoy, ENandBoy));
				PS.CreateBall = true;


			}
		}
		else if(Level == 3|| LevelStateInput.OnWhichLevel == 3){
			if (!PS.getGameArea3().getArrowCreateArea().contains(screenX, Gdx.graphics.getHeight() - screenY)) {
				StartPointToCreateBall = false;

			}
			if (PS.getGameArea3().getArrowCreateArea().contains(screenX, Gdx.graphics.getHeight() - screenY)) {
				StartPointToCreateBall = true;
				isTouchOnArea = true;
				PS.getGameArea3().PushNewBall(new MainPlayer(screenX - ((Gdx.graphics.getWidth() / 10) - (Gdx.graphics.getWidth() / 20)), (Gdx.graphics.getHeight() - screenY) - ((Gdx.graphics.getWidth() / 10) - (Gdx.graphics.getWidth() / 20)), ENandBoy, ENandBoy));
				PS.CreateBall = true;


			}
		}
		
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {

		if(Level == 1|| LevelStateInput.OnWhichLevel == 1) {
			if (PS.getGameArea().GetStackToCheckEmptyOrNot() != 0) {

				PS.getGameArea().GetTopElementOfStackToBuildArrow().isTouchONBallArea = false;
				PS.getGameArea().GetTopElementOfStackToBuildArrow().TopIsSent = true;

				PS.getGameArea().GetTopElementOfStackToBuildArrow().setTouchUp(true);
			}
		}
		else if(Level == 2|| LevelStateInput.OnWhichLevel == 2){
			if (PS.getGameArea2().GetStackToCheckEmptyOrNot() != 0) {

				PS.getGameArea2().GetTopElementOfStackToBuildArrow().isTouchONBallArea = false;
				PS.getGameArea2().GetTopElementOfStackToBuildArrow().TopIsSent = true;

				PS.getGameArea2().GetTopElementOfStackToBuildArrow().setTouchUp(true);
			}
		}
		else if(Level == 3|| LevelStateInput.OnWhichLevel == 3){
			if (PS.getGameArea3().GetStackToCheckEmptyOrNot() != 0) {

				PS.getGameArea3().GetTopElementOfStackToBuildArrow().isTouchONBallArea = false;
				PS.getGameArea3().GetTopElementOfStackToBuildArrow().TopIsSent = true;

				PS.getGameArea3().GetTopElementOfStackToBuildArrow().setTouchUp(true);
			}
		}
		//
		/*		PS.getMainPlayer().isTouchONBallArea = false;
		isTouchOnArea = false;
		PS.getMainPlayer().PushTheBall(UzaklıkX, UzaklıkY);
		PS.getMainPlayer().SetCreatedBall(true);
	*/	return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		System.out.println("LEVELİNİZ: "+ Level);
		if(Level == 1|| LevelStateInput.OnWhichLevel == 1) {
			if (StartPointToCreateBall == true && PS.getGameArea().getArrowCreateArea().contains(screenX, Gdx.graphics.getHeight() - screenY)) {


				if (isTouchOnArea) {
					PS.getGameArea().GetTopElementOfStackToBuildArrow().isTouchONBallArea = true;

					if (PS.getGameArea().GetTopElementOfStackToBuildArrow().isTouchONBallArea) {
						UzaklıkX = screenX - (PS.getGameArea().GetTopElementOfStackToBuildArrow().GetLineArrowStartPointX());
						UzaklıkY = (Gdx.graphics.getHeight() - screenY) - (PS.getGameArea().GetTopElementOfStackToBuildArrow().GetLineArrowStartPointY());
						PS.getGameArea().GetTopElementOfStackToBuildArrow().UzaklıkX = UzaklıkX;
						PS.getGameArea().GetTopElementOfStackToBuildArrow().UzaklıkY = UzaklıkY;
						PS.getGameArea().GetTopElementOfStackToBuildArrow().SetLineArrowEndPointX(2 * (screenX - UzaklıkX));
						PS.getGameArea().GetTopElementOfStackToBuildArrow().SetLineArrowEndPointY(2 * ((Gdx.graphics.getHeight() - screenY) - (UzaklıkY)));
					}
				}

			}
		}


			else if(Level == 2|| LevelStateInput.OnWhichLevel == 2){
				if (StartPointToCreateBall == true && PS.getGameArea2().getArrowCreateArea().contains(screenX, Gdx.graphics.getHeight() - screenY)) {

						System.out.println("birinci katmana girdi");
					if (isTouchOnArea) {
						PS.getGameArea2().GetTopElementOfStackToBuildArrow().isTouchONBallArea = true;
						System.out.println("ikinci katmana girdi");

						if (PS.getGameArea2().GetTopElementOfStackToBuildArrow().isTouchONBallArea) {
							System.out.println("üçüncü katmana girdi");

							UzaklıkX = screenX - (PS.getGameArea2().GetTopElementOfStackToBuildArrow().GetLineArrowStartPointX());
							UzaklıkY = (Gdx.graphics.getHeight() - screenY) - (PS.getGameArea2().GetTopElementOfStackToBuildArrow().GetLineArrowStartPointY());
							PS.getGameArea2().GetTopElementOfStackToBuildArrow().UzaklıkX = UzaklıkX;
							PS.getGameArea2().GetTopElementOfStackToBuildArrow().UzaklıkY = UzaklıkY;
							PS.getGameArea2().GetTopElementOfStackToBuildArrow().SetLineArrowEndPointX(2 * (screenX - UzaklıkX));
							PS.getGameArea2().GetTopElementOfStackToBuildArrow().SetLineArrowEndPointY(2 * ((Gdx.graphics.getHeight() - screenY) - (UzaklıkY)));
						}
					}

				}
			}





		else if(Level == 3|| LevelStateInput.OnWhichLevel == 3){
			if (StartPointToCreateBall == true && PS.getGameArea3().getArrowCreateArea().contains(screenX, Gdx.graphics.getHeight() - screenY)) {

				System.out.println("birinci katmana girdi");
				if (isTouchOnArea) {
					PS.getGameArea3().GetTopElementOfStackToBuildArrow().isTouchONBallArea = true;
					System.out.println("ikinci katmana girdi");

					if (PS.getGameArea3().GetTopElementOfStackToBuildArrow().isTouchONBallArea) {
						System.out.println("üçüncü katmana girdi");

						UzaklıkX = screenX - (PS.getGameArea3().GetTopElementOfStackToBuildArrow().GetLineArrowStartPointX());
						UzaklıkY = (Gdx.graphics.getHeight() - screenY) - (PS.getGameArea3().GetTopElementOfStackToBuildArrow().GetLineArrowStartPointY());
						PS.getGameArea3().GetTopElementOfStackToBuildArrow().UzaklıkX = UzaklıkX;
						PS.getGameArea3().GetTopElementOfStackToBuildArrow().UzaklıkY = UzaklıkY;
						PS.getGameArea3().GetTopElementOfStackToBuildArrow().SetLineArrowEndPointX(2 * (screenX - UzaklıkX));
						PS.getGameArea3().GetTopElementOfStackToBuildArrow().SetLineArrowEndPointY(2 * ((Gdx.graphics.getHeight() - screenY) - (UzaklıkY)));
					}
				}

			}
		}

		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	public PlayState getPS() {
		return PS;
	}

	public void setPS(PlayState pS) {
		PS = pS;
	}

}
