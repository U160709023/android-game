package com.wallybally.InputHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.wallybally.states.LevelState;
import com.wallybally.states.MenuState;
import com.wallybally.states.PlayState;

public class LevelStateInput implements InputProcessor {

    LevelState LS;
    int Level;
   public static int OnWhichLevel = 0;

    public LevelStateInput(LevelState LS, int Level){ this.LS = LS; this.Level = Level; }
    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        if(LS.getLevel1ScreenRect().contains(screenX, Gdx.graphics.getHeight() - screenY) && (Level == 1 || Level == 2 || Level == 3) && MenuState.isClickedStarButton){
            OnWhichLevel = 1;
            LS.getSm().PushState(new PlayState(LS.getSm(),Level));


        }if(LS.getLevel2ScreenRect().contains(screenX, Gdx.graphics.getHeight() - screenY) && (Level == 2 || Level ==3)&& MenuState.isClickedStarButton){
            OnWhichLevel = 2;

            LS.getSm().PushState(new PlayState(LS.getSm(),Level));


        } if(LS.getLevel3ScreenRect().contains(screenX, Gdx.graphics.getHeight() - screenY) && Level == 3 && MenuState.isClickedStarButton){
            OnWhichLevel = 3;

            LS.getSm().PushState(new PlayState(LS.getSm(), Level));
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
