package com.wallybally.game;

import java.util.prefs.BackingStoreException;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wallybally.ImageLoader.ImageLoader;
import com.wallybally.InputHandler.GameOverInput;
import com.wallybally.InputHandler.MenuStateInput;
import com.wallybally.InputHandler.PlayStateInput;
import com.wallybally.InputHandler.RecordHandler;
import com.wallybally.states.AddGameOver;
import com.wallybally.states.GameOverState;
import com.wallybally.states.LevelState;
import com.wallybally.states.MenuState;
import com.wallybally.states.PlayState;
import com.wallybally.states.StateManager;


public  class WallyBally extends ApplicationAdapter {
	public static int Width= 480;
	public static int Height = 720;
	public static String Title = "Wally Bally";
	private static StateManager SM;
	public static SpriteBatch SB;
	public boolean oyunbitti = false;
	public static int Level;


	public  boolean firstGame = true;
	public  boolean hundredPoint = true;
	public  boolean thousandPoint = true;
	public  boolean tenthousandPoint= true;
	public  boolean unlockWholeLevel = true;



	//////////////////////////////////////////////////////////////////

	//	SpriteBatch batch()
	//Texture img;

	 AddGameOver addGameOver;
	public WallyBally(AddGameOver addGameOver ){
	this.addGameOver = addGameOver;
	addGameOver.GirisYap();
		//this.thread.start();
	}


	public int getLevel() {
		return Level;
	}

	public void setLevel(int level) {
		Level = level;
	}

	@Override
	public void create () {
		Level = 1;
		RecordHandler.load();
		ImageLoader.load();
		System.out.println("get first game" + RecordHandler.getFirstgame());
		System.out.println("coin miktarı" + RecordHandler.getCoin());
		try {
			RecordHandler.setFirstGame(0);
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
		SB = new SpriteBatch();
		SM = new StateManager();
	//	SM.PushState(new PlayState(SM));
		SM.PushState(new MenuState(SM));
		System.out.println("oyun oluşturuldu");

	
		
		//thread.start();
		//batch = new SpriteBatch();
		//img = new Texture("badlogic.jpg");
	}

	@Override
	public void render () {


		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		SB.begin();

		//	SB.draw(ImageLoader.ForBGClearREG, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		SB.draw(ImageLoader.ForBGClearREG, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		SB.end();
		SM.render(SB);
		try {
			SM.update(Gdx.graphics.getDeltaTime());
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}

		//	System.out.println("oyun çiziliyor");
		if (PlayState.isGameEnd){
			if(RecordHandler.getCoin() >= 0 && RecordHandler.getFirstgame() == 0){ // ilk oyunu oyna

				addGameOver.ilkOyun();
				firstGame = false;

				//ilk oyun, 100 puanı gç,1000 puanı geç,10 bin puanı geç,tüm levelleri aç
			}
			try {
				addGameOver.gameEnded();
				PlayState.isGameEnd = false;

				if(RecordHandler.getCoin() >= 100 && RecordHandler.getFirstgame() == 1){ // 100 puanı geç
					addGameOver.hundredPoint();
					hundredPoint = false;

					//ilk oyun, 100 puanı gç,1000 puanı geç,10 bin puanı geç,tüm levelleri aç
				}
				if(RecordHandler.getCoin() >= 1000 && RecordHandler.getFirstgame() == 2){ // bin puanı geç
					addGameOver.thousandPoint();
					thousandPoint = false;
					//ilk oyun, 100 puanı gç,1000 puanı geç,10 bin puanı geç,tüm levelleri aç
				}
				if(RecordHandler.getCoin() >= 10000 && RecordHandler.getFirstgame() == 3){ // 10 bin puanı geç
					addGameOver.ten_thousandPoint();
					tenthousandPoint = false;

					//ilk oyun, 100 puanı gç,1000 puanı geç,10 bin puanı geç,tüm levelleri aç
				}
				System.out.println("level ="+ Level + "first game" + RecordHandler.getFirstgame());
				if(RecordHandler.getCoin()>= 35  && RecordHandler.getFirstgame() == 4){ // tüm levelleri aç
					addGameOver.Unlock_WholeLevels();
					System.out.println("son achievement açılacak");
					unlockWholeLevel = false;
					//ilk oyun, 100 puanı gç,1000 puanı geç,10 bin puanı geç,tüm levelleri aç
				}
			}catch (Exception e){
			}
		}
		if(MenuState.isClickedStarButton){
			try{addGameOver.InGame();
		}catch (Exception e) {
			}
			}
		if(GameOverInput.clickedWatchVideoButton){
			addGameOver.WatchVideo();
		}
			//System.out.println("isclicled ? "+ MenuState.isClickedStarButton);

		//Gdx.gl.glClearColor(1, 0, 0, 1);
		//Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		//batch.begin();
		//batch.draw(img, 0, 0
	}
	
	@Override
	public void dispose () {
	//	batch.dispose();
		//img.dispose();
		System.out.println("oyun yok edildi");
	
		SB.dispose();

		ImageLoader.dispose();

	}

}
