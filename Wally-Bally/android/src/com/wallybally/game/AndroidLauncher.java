package com.wallybally.game;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.games.Games;
import com.google.android.gms.internal.measurement.zzx;
import com.google.android.gms.measurement.internal.zzga;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.wallybally.InputHandler.GameOverInput;
import com.wallybally.InputHandler.MenuStateInput;
import com.wallybally.InputHandler.RecordHandler;
import com.wallybally.states.AddGameOver;
import com.wallybally.states.MenuState;
import com.wallybally.states.PlayState;
import com.google.android.gms.games.achievement.Achievement;


import java.nio.BufferUnderflowException;
import java.util.prefs.BackingStoreException;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;


public class AndroidLauncher extends AndroidApplication implements RewardedVideoAdListener , AddGameOver, GoogleApiClient.OnConnectionFailedListener {
	private static final String   TAG = "AndroidLauncher";
	public AdView bannerAdView;
	private static final int RC_ACHIEVEMENT_UI = 9003;
	boolean cannotSuccessForSıgnIn = true;
	public RewardedVideoAd watchVideoAdd;
	GoogleSignInClient googleSignInClient;
	GoogleApiClient mGoogleApiClient;
	int RC_SIGN_IN = 111;
	RelativeLayout relativeLayfout;
	int btn_request = 199;
	Button signOutButton;
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		MobileAds.initialize(this, "ca-app-pub-9282553803338393~4758570100");

		watchVideoAdd = MobileAds.getRewardedVideoAdInstance(this
		);
		watchVideoAdd.setRewardedVideoAdListener(this);

		loadRewardedVideoAd();
		Bundle bundle = new Bundle();

		//SignInButton btn = new SignInButton(this);
		RelativeLayout.LayoutParams paramsbtn = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
		//paramsbtn.setMargins(10,500,5,0);

		 relativeLayfout = new RelativeLayout(this);
		View gameView = initializeForView(new WallyBally(this),config);
        relativeLayfout.addView(gameView);

		bannerAdView = new AdView(this);


		bannerAdView.setAdListener(new AdListener(){
			@Override
			public void onAdLoaded(){
				Log.i(TAG,"ad loaded..");
			}
		});
		bannerAdView.setAdSize(AdSize.SMART_BANNER);
		bannerAdView.setAdUnitId("ca-app-pub-9282553803338393/3273920434");
		AdRequest.Builder builder = new AdRequest.Builder();
		builder.addTestDevice("609F69F3529008B7A7D4C9521B50E163");
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				WRAP_CONTENT,
				WRAP_CONTENT

		);
		relativeLayfout.addView(bannerAdView,params);
		bannerAdView.loadAd(builder.build());


		setContentView(relativeLayfout);




		LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
		View signInLayout = layoutInflater.inflate(R.layout.activity_my_deneme,relativeLayfout,true);
		SignInButton signInButton = signInLayout.findViewById(R.id.signInButton);





		//GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
		//		.requestIdToken("941341608528-ktlbb99vltu7c636ljngfr8t04lpisau.apps.googleusercontent.com")
		//		.requestEmail().build();


	//	GoogleSignInOptions gso = new GoogleSignInOptions
	//			.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
	//			.requestScopes(Games.SCOPE_GAMES_LITE)
	//			.requestEmail()
	//			.build();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(Games.SCOPE_GAMES_LITE)
                .requestEmail()
                .build();
		//GoogleSignInOptions  gso =
		//		new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
		//				.requestScopes(Drive.SCOPE_APPFOLDER)
		//				.build();
//

		googleSignInClient = GoogleSignIn.getClient(AndroidLauncher.this,gso);
		signInButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				switch (view.getId()) {
					case R.id.signInButton:
						signIn();
						break;
					// ...
				}

			}
		});




	}
	private void signIn(){
		Intent SigninIntent = googleSignInClient.getSignInIntent();
		startActivityForResult(SigninIntent,RC_SIGN_IN);


	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);


		// Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
		if (requestCode == RC_SIGN_IN) {
			// The Task returned from this call is always completed, no need to attach
			// a listener.
			Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
			handleSignInResult(task);
		}
	}

	private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
		try {
			GoogleSignInAccount account = completedTask.getResult(ApiException.class);
			Dialog dialog = new Dialog(AndroidLauncher.this);
			dialog.setTitle("Succesfull");
			TextView textView = new TextView(this);
			textView.setText("You signed in successfully");
			cannotSuccessForSıgnIn = false;
			dialog.setContentView(textView);
			dialog.show();
			// Signed in successfully, show authenticated UI.
		} catch (ApiException e) {
			// The ApiException status code indicates the detailed failure reason.
			// Please refer to the GoogleSignInStatusCodes class reference for more information.
			Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
			Dialog dialog = new Dialog(AndroidLauncher.this);
			dialog.setTitle("Something wrong");
			TextView textView = new TextView(this);
			textView.setText("You could not sign in with google account failed code=" + e.getStatusCode());
			dialog.setContentView(textView);
			dialog.show();
		}
	}

	private void loadRewardedVideoAd() {

		if(!watchVideoAdd.isLoaded()){
			Log.i(TAG,"yüklenecek");
			watchVideoAdd.loadAd("ca-app-pub-3940256099942544/5224354917",new AdRequest.Builder().build());

		}

		}

	public void onPost(){


	}

	@Override
	protected void onStart() {
		super.onStart();
	signInSilently();
	}

	@Override
	public void onRewardedVideoAdLoaded() {

	}

	@Override
	public void onRewardedVideoAdOpened() {

	}

	@Override
	public void onRewardedVideoStarted() {

	}

	@Override
	public void onRewardedVideoAdClosed() {
		loadRewardedVideoAd();

	}

	@Override
	public void onRewarded(RewardItem rewardItem) {


	}

	@Override
	public void onRewardedVideoAdLeftApplication() {



	}

	@Override
	public void onRewardedVideoAdFailedToLoad(int i) {

	}

	@Override
	public void onRewardedVideoCompleted() {
		try {
			RecordHandler.setCoin(PlayState.kazanilanpuan + RecordHandler.getCoin());
			PlayState.kazanilanpuan = 0;
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void CreateAds() {

	}

	@Override
	public void gameEnded() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				bannerAdView.setVisibility(View.VISIBLE);
			}
		});
		try{
			//imgButton.setVisibility(View.VISIBLE);


		}catch (Exception e){

		}

	}

	@Override
	public void InGame() {
		try{
			if(bannerAdView.getVisibility() == AdView.VISIBLE)
				bannerAdView.setVisibility(View.INVISIBLE);
		}catch (Exception e){

		}

	}

	@Override
	public void InMenu() {

	}

	@Override
	public void WatchVideo() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				watchVideoAdd.show();
				GameOverInput.clickedWatchVideoButton = false;

			}
		});


	}

	@Override
	public void ilkOyun() {
		if(!cannotSuccessForSıgnIn){
		Games.getAchievementsClient(this, GoogleSignIn.getLastSignedInAccount(this)).unlock("CgkI0JTU4rIbEAIQAA");
		System.out.println("first step is unclocked");
			try {
				RecordHandler.setFirstGame(1);
			} catch (BackingStoreException e) {
				e.printStackTrace();
			}
			showAchievements(R.string.achievement_first_steps);
		}
	}

	private void showAchievements(final int UI) {
		Games.getAchievementsClient(this, GoogleSignIn.getLastSignedInAccount(this))
				.getAchievementsIntent()
				.addOnSuccessListener(new OnSuccessListener<Intent>() {
					@Override
					public void onSuccess(Intent intent) {
						startActivityForResult(intent, UI);
					}
				});
	}

	@Override
	public void hundredPoint() {
		if(!cannotSuccessForSıgnIn){
			try {
				RecordHandler.setFirstGame(2);
			} catch (BackingStoreException e) {
				e.printStackTrace();
			}
			Games.getAchievementsClient(this, GoogleSignIn.getLastSignedInAccount(this)).unlock("CgkI0JTU4rIbEAIQAQ");
			System.out.println("burayı çağırdı");

			showAchievements(R.string.achievement_welcome_to_100_club);
		}




	}

	@Override
	public void thousandPoint() {
		if(!cannotSuccessForSıgnIn){


			try {
				RecordHandler.setFirstGame(3);
			} catch (BackingStoreException e) {
				e.printStackTrace();
			}

			Games.getAchievementsClient(this, GoogleSignIn.getLastSignedInAccount(this)).unlock("CgkI0JTU4rIbEAIQAg");
			System.out.println("unlocked thousand");

			showAchievements(R.string.achievement_high_1000);
		}

	}

	@Override
	public void ten_thousandPoint() {
		if(!cannotSuccessForSıgnIn){
			try {
				RecordHandler.setFirstGame(4);
			} catch (BackingStoreException e) {
				e.printStackTrace();
			}

			Games.getAchievementsClient(this, GoogleSignIn.getLastSignedInAccount(this)).unlock("CgkI0JTU4rIbEAIQAw");
			System.out.println("unlocked ten thousand");

			showAchievements(R.string.achievement_10k);
		}
	}

	@Override
	public void Unlock_WholeLevels() {
		if(!cannotSuccessForSıgnIn){
			try {
				RecordHandler.setFirstGame(5);
			} catch (BackingStoreException e) {
				e.printStackTrace();
			}
			System.out.println("unlocked whole level");
			Games.getAchievementsClient(this, GoogleSignIn.getLastSignedInAccount(this)).unlock("CgkI0JTU4rIbEAIQBA");
			System.out.println("burayı çağırdı");

			showAchievements(R.string.achievement_all_clear);
		}

	}

	@Override
	public void GirisYap() {
	}

	@Override
	public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

	}


	class adsImplement extends AsyncTask<Void,Void,Void>{


		@Override
		protected Void doInBackground(Void... voids) {


			return null;
		}
	}
	private void signInSilently() {
		GoogleSignInOptions signInOptions = GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN;
		GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
		if (GoogleSignIn.hasPermissions(account, signInOptions.getScopeArray())) {
			// Already signed in.
			// The signed in account is stored in the 'account' variable.
			GoogleSignInAccount signedInAccount = account;
		} else {
			// Haven't been signed-in before. Try the silent sign-in first.
			GoogleSignInClient signInClient = GoogleSignIn.getClient(this, signInOptions);
			signInClient
					.silentSignIn()
					.addOnCompleteListener(
							this,
							new OnCompleteListener<GoogleSignInAccount>() {
								@Override
								public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
									if (task.isSuccessful()) {
										// The signed in account is stored in the task's result.
										GoogleSignInAccount signedInAccount = task.getResult();
									} else {
										// Player will need to sign-in explicitly using via UI.
										// See [sign-in best practices](http://developers.google.com/games/services/checklist) for guidance on how and when to implement Interactive Sign-in,
										// and [Performing Interactive Sign-in](http://developers.google.com/games/services/android/signin#performing_interactive_sign-in) for details on how to implement
										// Interactive Sign-in.
									}
								}
							});
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		signInSilently();
	}

}
