package com.wallybally.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.wallybally.game.WallyBally;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = WallyBally.Width;
		config.height = WallyBally.Height	;
		config.title = WallyBally.Title;
		config.foregroundFPS = 5000;
		new LwjglApplication(new WallyBally(), config);
	}
}
